# Assignment 9 - Timers and Interrupts

## By Sharee Acosta

Answers to the following questions in readme.md file:
	-How long of a delay are you using? Explain why you chose the settings you did.
		I'm using a 40 ms delay because I replaced the 40 ms delay from the previous code for the bouncy ball (in game 3).
	-How did you test your program to verify that it still worked? Be specific. Describing the various tests you used will get you off to a great start.
		I tested my program by running it to see if it still worked.  I copied the interrupt code from the powerpoint slides and modified it to the time delay I wanted.  The program ran the same as if I never changed any part of the program.
[https://youtu.be/JH9lG9lGQug](https://youtu.be/JH9lG9lGQug "Bouncy Ball")


### Documentation
I used the code that C2C Talosaga and I created for lab 4.  C2C Mireles helped me replace the interrupt code in the right places.  He also helped me debug my calculations for the counts.
