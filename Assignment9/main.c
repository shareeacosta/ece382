/*--------------------------------------------------------------------
Name: Sharee Acosta (Anthony Talosaga's initial file from assignment 7 used for Lab 4)
Date: 30 Oct 2016
Course: ECE382
File: main.c
HW: Assignment9

Purp: Use a timer interrupt subsystem for the delay on my bouncy ball.

Doc: C2C Talosaga during Lab 4 to create file.  C2C Mireles helped explain to me how to include the code in the correct place.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h>
#include "ball.h"

extern void initMSP();
extern void initLCD();
extern void clearScreen();
extern void Delay160ms();
extern void Delay40ms();
extern void drawBox(int x, int y, int color);
extern void drawPaddle(int x, int y, int color);
extern void setBright(unsigned int x);

#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define		MAIN_BUTTON		(P1IN & BIT3)
#define		SCREEN_HEIGHT	320	//Screen size from Lab3
#define		SCREEN_WIDTH	240
#define		GREEN			0x07E0
#define		WHITE			0xFFFF
#define		PINK			0xFB56
#define		CLEAR			0x0000
#define		VELOCITY		2
#define		XSTART			10
#define		YSTART			10
#define		RADIUS			5

char flag;
#pragma vector=TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR()
{
    TA0CTL &= ~TAIFG;            // clear interrupt flag
    flag = 1;
}

void main() {

	// === Initialize system ================================================
	IFG1=0; /* clear interrupt flag1 */
	WDTCTL=WDTPW+WDTHOLD; /* stop WD */
	BCSCTL1 = CALBC1_8MHZ;      // Set SMCLK 8 MHz
	DCOCTL = CALDCO_8MHZ;

	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();
	TA0CTL |= TACLR;
	TA0CTL &= ~TAIFG;
	TA0CCR0 = 0x9c40; // for 40 ms;
//	8x10^6 clks x 1 cnt x 0.04 s
//	----------------------------  = 40,000 cnts = 0x9c40
//     1 sec    x 4 clks x 1 TAR
	TA0CTL |= ID_3 | TASSEL_2 | MC_0;
	__enable_interrupt();

	int game = 0;
	int mode = 1;
	unsigned int bright = 127;

/***************************************************************************/
	while(1) {

	/*=======================================================*/

	if (game == 1) {
	
	int condition = TRUE;
	int pause = FALSE;

	ball myBall = createBall(XSTART, YSTART, VELOCITY, VELOCITY, RADIUS);  //Let's create a ball
	paddle myPaddle = createPaddle(SCREEN_WIDTH/2, SCREEN_HEIGHT-10, 80);

	drawBox(myBall.position.x, myBall.position.y, GREEN);
	drawPaddle(myPaddle.position.x, myPaddle.position.y, PINK);


	while(condition) {
		Delay40ms();

		if (pause == FALSE) {
			drawBox(myBall.position.x, myBall.position.y, CLEAR);

			if (myBall.position.y + 7 >= myPaddle.position.y) {
				if (myBall.position.x + 2 > myPaddle.position.x - 40 && myBall.position.x - 2 < myPaddle.position.x + 40) {
					myBall.velocity.y = -myBall.velocity.y;
					myBall = moveBall(myBall, SCREEN_WIDTH, SCREEN_HEIGHT);
				}
				else {
					condition = FALSE;
				}
			}
			else {
				myBall = moveBall(myBall, SCREEN_WIDTH-2, SCREEN_HEIGHT);
			}

			if (MAIN_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				pause = TRUE;
			}
		}

		drawBox(myBall.position.x, myBall.position.y, GREEN);
		drawPaddle(myPaddle.position.x, myPaddle.position.y, PINK);

		if (MAIN_BUTTON == 0) {
			Delay160ms();
			Delay160ms();
			Delay160ms();
			Delay160ms();
			pause = FALSE;
		}

		if (LEFT_BUTTON == 0) {
			if (hitLeft(myPaddle.position.x, 40) == FALSE) {
				drawPaddle(myPaddle.position.x, myPaddle.position.y, CLEAR);
				myPaddle.position.x -= 5;
				drawPaddle(myPaddle.position.x, myPaddle.position.y, PINK);
			}

		}
		if (RIGHT_BUTTON == 0) {
			if (hitRight(myPaddle.position.x, 40, SCREEN_WIDTH) == FALSE) {
				drawPaddle(myPaddle.position.x, myPaddle.position.y, CLEAR);
				myPaddle.position.x += 5;
				drawPaddle(myPaddle.position.x, myPaddle.position.y, PINK);
			}
		}
		if (UP_BUTTON == 0) {
			if (myBall.velocity.x < 0) {myBall.velocity.x -= 1;}
			else {myBall.velocity.x += 1;}
			if (myBall.velocity.y < 0) {myBall.velocity.y -= 1;}
			else {myBall.velocity.y += 1;}
			Delay160ms();
			Delay160ms();
		}
		if (DOWN_BUTTON == 0) {
			if (myBall.velocity.x < 0) {myBall.velocity.x += 1;}
			else {myBall.velocity.x -= 1;}
			if (myBall.velocity.y < 0) {myBall.velocity.y += 1;}
			else {myBall.velocity.y -= 1;}
			Delay160ms();
			Delay160ms();
		}
	} //first_game loop

	} //first_game end

	/*=====================================================================*/

	if (game == 2) {

	int mode = 1;
	int x = SCREEN_WIDTH / 2;
	int y = SCREEN_HEIGHT / 2;
	int velocity = 7;
	int condition2 = TRUE;

	drawBox(x, y, GREEN);

	while(condition2) {
		if (mode == 2) {
			if (UP_BUTTON == 0) {
				Delay160ms();
				if (hitTop(y, 10) == FALSE) {
					drawBox(x, y, CLEAR);
					y -= velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (DOWN_BUTTON == 0) {
				Delay160ms();
				if (hitBottom(y, 10, SCREEN_HEIGHT) == FALSE) {
					drawBox(x, y, CLEAR);
					y += velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (LEFT_BUTTON == 0) {
				Delay160ms();
				if (hitLeft(x, 10) == FALSE) {
					drawBox(x, y, CLEAR);
					x -= velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (RIGHT_BUTTON == 0) {
				Delay160ms();
				if (hitRight(x, 10, SCREEN_WIDTH) == FALSE) {
					drawBox(x, y, CLEAR);
					x += velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (MAIN_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				drawBox(x, y, GREEN);
				mode = 3;
			}
		}
		if (mode == 1) {
			drawBox(x, y, GREEN);

			if (UP_BUTTON == 0) {
				Delay160ms();
				if (hitTop(y, 10) == FALSE) {
					y = y - velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (DOWN_BUTTON == 0) {
				Delay160ms();
				if (hitBottom(y, 10, SCREEN_HEIGHT) == FALSE) {
					y = y + velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (LEFT_BUTTON == 0) {
				Delay160ms();
				if (hitLeft(x, 10) == FALSE) {
					x = x - velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (RIGHT_BUTTON == 0) {
				Delay160ms();
				if (hitRight(x, 10, SCREEN_WIDTH) == FALSE) {
					x = x + velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (MAIN_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				drawBox(x, y, WHITE);
				mode = 2;
			}
		}

		if (mode == 3) {
			drawBox(x, y, PINK);

			if (UP_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				condition2 = FALSE;
			}
			if (MAIN_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				mode = 1;
			}
		}

	} //second_game while end

	} //second_game end

	/*====================================================================*/

	if (game == 3) {

	int condition = TRUE;
	int pause = FALSE;
	int mode = 1;
	int funct = 0;

	ball myBall = createBall(XSTART, YSTART, VELOCITY, VELOCITY, RADIUS);  //Let's create a ball
	drawBox(myBall.position.x, myBall.position.y, GREEN);
	TA0CTL |= TAIE;
	TA0CTL |= MC_1;
	while(condition) {
		flag = 0;
		while (flag!=1);
//		Delay40ms();

		if (pause == FALSE) {
			if (mode) {
				drawBox(myBall.position.x, myBall.position.y, CLEAR);
				myBall = moveBall(myBall, SCREEN_WIDTH-2, SCREEN_HEIGHT);
				drawBox(myBall.position.x, myBall.position.y, GREEN);

				if (RIGHT_BUTTON == 0) {
					Delay160ms();
					mode = 0;
				}
			}
			if (mode == 0) {
				myBall = moveBall(myBall, SCREEN_WIDTH-2, SCREEN_HEIGHT);
				drawBox(myBall.position.x, myBall.position.y, GREEN);

				if (RIGHT_BUTTON == 0) {
					Delay160ms();
					mode = 1;
				}
			}
			if (MAIN_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				pause = TRUE;
				funct = 1;
			}
		}

		if (MAIN_BUTTON == 0) {
			Delay160ms();
			Delay160ms();
			Delay160ms();
			Delay160ms();
			pause = FALSE;
			funct = 0;
		}

		if (funct) {
			if (LEFT_BUTTON == 0) {
				drawBox(myBall.position.x, myBall.position.y, CLEAR);
				myBall.position.x -= 5;
				drawBox(myBall.position.x, myBall.position.y, GREEN);
			}
			if (RIGHT_BUTTON == 0) {
				drawBox(myBall.position.x, myBall.position.y, CLEAR);
				myBall.position.x += 5;
				drawBox(myBall.position.x, myBall.position.y, GREEN);
			}
			if (UP_BUTTON == 0) {
				drawBox(myBall.position.x, myBall.position.y, CLEAR);
				myBall.position.y -= 5;
				drawBox(myBall.position.x, myBall.position.y, GREEN);
			}
			if (DOWN_BUTTON == 0) {
				drawBox(myBall.position.x, myBall.position.y, CLEAR);
				myBall.position.y += 5;
				drawBox(myBall.position.x, myBall.position.y, GREEN);
			}
		}
		else {
			if (LEFT_BUTTON == 0) {condition = FALSE;}

			if (UP_BUTTON == 0) {
				if (myBall.velocity.x < 0) {myBall.velocity.x -= 1;}
				else {myBall.velocity.x += 1;}
				if (myBall.velocity.y < 0) {myBall.velocity.y -= 1;}
				else {myBall.velocity.y += 1;}
				Delay160ms();
				Delay160ms();
			}
			if (DOWN_BUTTON == 0) {
				if (myBall.velocity.x < 0) {myBall.velocity.x += 1;}
				else {myBall.velocity.x -= 1;}
				if (myBall.velocity.y < 0) {myBall.velocity.y += 1;}
				else {myBall.velocity.y -= 1;}
				Delay160ms();
				Delay160ms();
			}
		}
	} //third_game loop

	} //third_game end

	/*====================================================================*/
	game = FALSE;

	if (mode) {
		if (MAIN_BUTTON == 0) {mode = 0;}
		if (UP_BUTTON == 0) { //game1_reset
			clearScreen();
			game = 1;

		} //game1_reset_end

		if (DOWN_BUTTON == 0) { //game2_reset
			clearScreen();
			game = 2;
		} //game2_reset_end

		if (RIGHT_BUTTON == 0) { //game3_reset
				clearScreen();
				game = 3;
		} //game3_reset_end
	}
	else {
		if (MAIN_BUTTON == 0) {mode = 1;}
		if (UP_BUTTON == 0) {
			Delay160ms();
			Delay160ms();
			bright += 10;
			setBright(bright);
		}
		if (DOWN_BUTTON == 0) {
			Delay160ms();
			Delay160ms();
			bright -= 10;
			setBright(bright);
		}
	}

	} //CPU trap

/*******************************************************************************/








}
