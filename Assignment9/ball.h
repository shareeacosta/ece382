/*--------------------------------------------------------------------
Name: Sharee Acosta (Anthony Talosaga's initial file from assignment 7 used for Lab 4)
Date: 30 Oct 2016
Course: ECE382
File: ball.h (from - Assign_7)
HW: Assignment9

Purp: Use a timer interrupt subsystem for the delay on my bouncy ball.

Doc: C2C Talosaga during Lab 4 to create file.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#ifndef BALL_H_
#define BALL_H_

/*-------------------------------------------*
 * Name: vector_2d
 * Author:	Anthony C Talosaga (given template)
 * Purpose:	stores a x-y location or normal integers
 * Inputs:	none
 * Outputs:	sturct vector2d
 *-------------------------------------------*/
typedef struct vector2d {
    int x;
    int y;
} vector_2d;

/*-------------------------------------------*
 * Name: ball
 * Author:	Anthony C Talosaga
 * Purpose:	defines a struct "ball" with
 * 			x-y position, x-y velocity, and
 * 			radius
 * Inputs:	none
 * Outputs:	sturct ball_d
 *-------------------------------------------*/
typedef struct ball_d {
	vector_2d position;
	vector_2d velocity;
	int radius;
} ball;

/*-------------------------------------------*
 * Name: paddle
 * Author:	Anthony C Talosaga
 * Purpose:	defines a struct "paddle" with
 * 			x-y position and length
 * Inputs:	none
 * Outputs:	sturct paddle_d
 *-------------------------------------------*/
typedef struct paddle_d {
	vector_2d position;
	int length;
} paddle;

/*-------------------------------------------*
 * Name: createBall
 * Author: Anthony C Talosaga
 * Purpose: create an instance of struct "ball"
 * Inputs: xPosition, yPosition, xVelocity, yVelocity, and Radius
 * Outputs: return an instance of the struct "ball"
 *-------------------------------------------*/
ball createBall(int xPosition, int yPosition, int xVelocity, int yVelocity, int Radius);

/*-------------------------------------------*
 * Name: createPaddle
 * Author: Anthony C Talosaga
 * Purpose: create an instance of struct "paddle"
 * Inputs: x and y position and length
 * Outputs: return an instance of the struct "paddle"
 *-------------------------------------------*/
paddle createPaddle(int x, int y, int length);

/*-------------------------------------------*
 * Name: moveBall
 * Author: Anthony C Talosaga
 * Purpose: to move an instance of struct "ball"
 * 			by changing its x- and y- position
 * Inputs: struct myBall, width_limit, height_limit
 * Outputs: return a new instance of struct myBall
 *-------------------------------------------*/
ball moveBall(ball myBall, int width_limit, int height_limit);

/*-------------------------------------------*
 * Name: hitLeft
 * Author: Anthony C Talosaga
 * Purpose: check if ball reached left edge
 * Inputs: xPosition and Radius
 * Outputs: return TRUE (1) if it reached left edge,
 * 			else return FALSE (0)
 *-------------------------------------------*/
char hitLeft(int xPosition, int Radius);

/*-------------------------------------------*
 * Name: hitRight
 * Author: Anthony C Talosaga
 * Purpose: check if ball reached right edge
 * Inputs: xPosition, Radius, and width_limit
 * Outputs: return TRUE (1) if it reached right edge,
 * 			else return FALSE (0)
 *-------------------------------------------*/
char hitRight(int xPosition, int Radius, int width_limit);

/*-------------------------------------------*
 * Name: hitTop
 * Author: Anthony C Talosaga
 * Purpose: check if ball reached top edge
 * Inputs: yPosition and Radius
 * Outputs: return TRUE (1) if it reached top edge,
 * 			else return FALSE (0)
 *-------------------------------------------*/
char hitTop(int yPosition, int Radius);

/*-------------------------------------------*
 * Name: hitBottom
 * Author: Anthony C Talosaga
 * Purpose: check if ball reached bottom edge
 * Inputs: yPosition, Radius, and height_limit
 * Outputs: return TRUE (1) if it reached bottom edge,
 * 			else return FALSE (0)
 *-------------------------------------------*/
char hitBottom(int yPosition, int Radius, int height_limit);

#endif /* BALL_H_ */
