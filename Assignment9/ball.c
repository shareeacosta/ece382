/*--------------------------------------------------------------------
Name: Sharee Acosta (Anthony Talosaga's initial file from assignment 7 used for Lab 4)
Date: 30 Oct 2016
Course: ECE382
File: ball.c (from - Assign_7)
HW: Assignment9

Purp: Use a timer interrupt subsystem for the delay on my bouncy ball.

Doc: C2C Talosaga during Lab 4 to create file.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include "ball.h"
#define TRUE 1
#define FALSE 0

ball createBall(int xPosition, int yPosition, int xVelocity, int yVelocity, int Radius) {
	ball newBall;
	newBall.position.x = xPosition;		//Setting the different fields in instance struct ball
	newBall.position.y = yPosition;		//to their corresponding values from the function inputs
	newBall.velocity.x = xVelocity;
	newBall.velocity.y = yVelocity;
	newBall.radius = Radius;

	return newBall;
}

paddle createPaddle(int x, int y, int length) {
	paddle newPaddle;
	newPaddle.position.x = x;
	newPaddle.position.y = y;
	newPaddle.length = length;

	return newPaddle;
}

ball moveBall(ball myBall, int width_limit, int height_limit) {
	myBall.position.x += myBall.velocity.x;		//First, it increments/decrements the x- and y- position of myBall
	myBall.position.y += myBall.velocity.y;

	if (hitLeft(myBall.position.x, myBall.radius)) {	//Invert velocity.x when hit left edge
		myBall.velocity.x = -myBall.velocity.x;
	}

	if (hitRight(myBall.position.x, myBall.radius, width_limit)) {		//Invert velocity.x when hit right edge
		myBall.velocity.x = -myBall.velocity.x;
	}

	if (hitTop(myBall.position.y, myBall.radius)) {		//Invert velocity.y when hit top edge
		myBall.velocity.y = -myBall.velocity.y;
	}

	if (hitBottom(myBall.position.y, myBall.radius, height_limit)) {	//Invert velocity.y when hit bottom edge
		myBall.velocity.y = -myBall.velocity.y;
	}

	return myBall;
}

/* Note: The four "collision detections" are tested at contact between screen edge and outer edge of circle.
 	 	 Thus, it will take account the radius of the circle. */
char hitLeft(int xPosition, int Radius) {
	if (xPosition-Radius <= 0) {return TRUE;}
	else {return FALSE;}
}

char hitRight(int xPosition, int Radius, int width_limit) {
	if (xPosition+Radius >= width_limit) {return TRUE;}
	else {return FALSE;}
}

char hitTop(int yPosition, int Radius) {
	if (yPosition-Radius <= 0) {return TRUE;}
	else {return FALSE;}
}

char hitBottom(int yPosition, int Radius, int height_limit) {
	if (yPosition+Radius >= height_limit) {return TRUE;}
	else {return FALSE;}
}







