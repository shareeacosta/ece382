/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 1 Dec - 9 Dec 2016
Course: ECE382
File: main.c
HW: Lab8

Purp: Move the robot around a maze using the ultrasonic sensors.

Doc: C2C Mireles helped me understand how changing the TACCR values affect the wheel speed and the set up for the required functionality.  C2C Hanson helped me debug my required functionality and A functionality.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h> 
#include "lab8.h"

void main(void) {
    WDTCTL = WDTPW | WDTHOLD;					// Stop watchdog timer
    init_MSP();
    while(1){
    	turnCenter();							// keep the sensor forward
    	__delay_cycles(50000);
    	moveForward();
    	checkDistance();
    	if(DIST < 55){							// if there is a wall in front, stop and look left
    		__delay_cycles(50000);
    		stop();
    		centerON();
    		__delay_cycles(50000);
    		turnLeft();
    		checkDistance();
    		if(DIST < 50){						// if there is a wall on the left, turn right
    			stop();
    			leftON();
    			__delay_cycles(50000);
				right();
    		}
    		else{								// no wall on the right, look right
    			__delay_cycles(50000);
    			stop();
    			turnRight();
    			checkDistance();
    			__delay_cycles(50000);
    			if(DIST > 500){					// if there is no wall in the distant, turn left
    				stop();
    				__delay_cycles(50000);
					left();
    			}
    			else{							// there a wall to the right, turn left
    				stop();
    				rightON();
    				__delay_cycles(50000);
					left();
    			}
    		}
    	}

    	else{									// if no wall, keep moving forward
    		__delay_cycles(10000);
    		}
    }
}


#pragma vector = PORT1_VECTOR
__interrupt void getDistance(void){
	if(P1IN & BIT4){							// on the rising edge, sets time to 0, set interrupt enable
		TA0R = 0;
		TA0CTL |= MC_1;
		P1IES |= BIT4;
	}
	else{										// on the falling edge, get the pulse length and convert to cm, clear inrrupt enable
		TA0CTL &= ~MC_1;
		DIST = (TA0R / 58) ;
		P1IES &= ~BIT4;
	}
	P1IFG &= ~BIT4;								// clear interrupt flag
}
