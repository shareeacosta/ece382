/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 1 Dec - 9 Dec 2016
Course: ECE382
File: lab8.c
HW: Lab8

Purp: Each function used to control the robot.

Doc: C2C Mireles helped me understand how changing the TACCR values affect the wheel speed and the set up for the required functionality.  C2C Hanson helped me debug my required functionality and A functionality.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430g2553.h>
#include "lab8.h"

void init_MSP(){
	WDTCTL = WDTPW|WDTHOLD;

	P1DIR = BIT0|BIT6;				// Set the LED as output
	P1SEL &= ~(BIT0|BIT6);
	P1SEL2 &= ~(BIT0|BIT6);
	P1OUT &= ~(BIT0|BIT6);

	P1DIR |= BIT2;					// set servo
	P1SEL |= BIT2;
	P1SEL2 &= ~BIT2;
	P1OUT &= ~BIT2;

	TA0CTL |= TASSEL_2|ID_0;		// configure for SMCLK
	TA0CTL &= ~TAIE;
	TA0CTL &= ~TAIFG;

	P1DIR &= ~BIT4;					// GPIO input P1.4 for servo
	P1SEL &= ~BIT4;

	P1IFG &= ~BIT4;					// interrupt on P1.4
	P1IES &= ~BIT4;
	P1IE |= BIT4;

	P2DIR |= BIT7;					// trigger on P2.7
	P2SEL &= ~BIT7;
	P2SEL2 &= ~BIT7;
	P2OUT &= ~BIT7;

	TA0CCTL1 |= OUTMOD_7;
	TA0CCR0 = 20000;					// want 20 ms roll over for servo


	P2DIR |= BIT1;                		// TA1CCR1 on P2.1
	P2SEL |= BIT1;
	P2OUT &= ~BIT1;
	TA1CTL |= TASSEL_2|MC_1|ID_0;       // configure for SMCLK

	P2DIR |= BIT4;                		// TA1CCR2 on P2.4
	P2SEL |= BIT4;
	P2OUT &= ~BIT4;


	P2DIR |= BIT2;
	P2SEL &= ~BIT2;                		// Left GPIO on P2.2
	P2OUT &= ~BIT2;

	P2DIR |= BIT3;
	P2SEL &= ~BIT3;                		// Right GPIO on P2.3
	P2OUT &= ~BIT3;

	P2DIR |= BIT0;
	P2SEL &= ~BIT0;                		// Left and right motor enable on P2.0
	P2OUT &= ~BIT0;


	TA1CCR0 = 1000;                		// set signal period to 1000 clock cycles (~1 millisecond)
	TA1CCR1 = 750;                		// set duty cycle to 75%
	TA1CCTL1 |= OUTMOD_3;       		// set TACCTL1 to Set / Reset mode

	TA1CCR2 = 750;             		   	// set duty cycle to 75%
	TA1CCTL2 |= OUTMOD_3;        		// set TACCTL2 to Set / Reset mode


	P2SEL &= ~BIT6;						// P2.6 as GPIO
	P2SEL2 &= ~BIT6;
	P2DIR &= ~BIT6;

	P2IFG &= ~BIT6;						// Clear interrupt flag on P2.6
	P2IE  |= BIT6;						// Enable P2.6 interrupt

	HIGH_2_LOW;							// set LED lights
	P1DIR |= BIT0 | BIT6;
	P1OUT &= ~(BIT0 | BIT6);

	TA0CCR0 = 16000;					// create a 16ms roll-over period

	TA0CTL |= TASSEL_2|ID_0;			// configure for SMCLK
	TA0CTL &= ~TAIE;
	TA0CTL &= ~TAIFG;

	TA0CCTL1 |= OUTMOD_7;
	TA0CCR0 = 20000;					// want 20 ms roll over for servo

	_enable_interrupt();
	return;
}

void moveForward(){						// enable motor to move forward
	TA1CCR1 = 700;
	TA1CCR2 = 700;
	P2OUT |= BIT2;
	P2OUT |= BIT3;
	P2OUT |= BIT0;
	return;
}

void left(){							// enable motor for a left turn
	P2OUT |= BIT2;
	P2OUT &= ~BIT3;
	P2OUT |= BIT0;
	__delay_cycles(270000);
	P2OUT &= ~BIT0;
	P2OUT &= ~BIT2;
	return;
}

void right(){							// enable motor for a right turn
	P2OUT &= ~BIT2;
	P2OUT |= BIT3;
	P2OUT |= BIT0;
	__delay_cycles(200000);
	P2OUT &= ~BIT0;
	P2OUT &= ~BIT3;
	return;
}

void stop(){							// disable motor
	P2OUT &= ~BIT0;
	return;
}

void turnLeft(){						// turn the servo to the left
	TA0CCR1 = 2700;
	TA0CTL |= MC_1;
	TA0CCTL1 |= OUTMOD_7;
	__delay_cycles(250000);
	TA0CCTL1 |= OUTMOD_0;
	TA0CTL &= ~MC_1;
}

void turnCenter(){						// turn the servo to the center
	TA0CCR1 = 1700;
	TA0CTL |= MC_1;
	TA0CCTL1 |= OUTMOD_7;
	__delay_cycles(200000);
	TA0CCTL1 |= OUTMOD_0;
	TA0CTL &= ~MC_1;
}

void turnRight(){						// turn the servo to the right
	TA0CCR1 = 580;
	TA0CTL |= MC_1;
	TA0CCTL1 |= OUTMOD_7;
	__delay_cycles(250000);
	TA0CCTL1 |= OUTMOD_0;
	TA0CTL &= ~MC_1;
}

void checkDistance(){					// calculate the distance of the object
	TA0CCR0 = 50000;
	P2OUT |= BIT7;
	__delay_cycles(10);
	P2OUT &= ~BIT7;
	__delay_cycles(10000);
	TA0CCR0 = 20000;
	return;
}

void centerON(){						// turn on both LEDs for debugging
	P1OUT |= (BIT0|BIT6);
	__delay_cycles(10000);
	P1OUT &= ~(BIT0|BIT6);
	return;
}

void rightON(){							// turn on right LED for debugging
	P1OUT |= BIT6;
	__delay_cycles(10000);
	P1OUT &= ~BIT6;
	return;
}

void leftON(){							// turn on left LED for debugging
	P1OUT |= BIT0;
	__delay_cycles(10000);
	P1OUT &= ~BIT0;
	return;
}

