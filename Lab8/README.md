# Lab 7 and 8 - A/D Conversion and Robot Maze - "Robot Sensing"

## By: Sharee Acosta

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Prelab 7](#prelab-7)
3. [Prelab 8](#prelab-8)
2. [Hardware](#hardware)
3. [Logic Analyzer](#logic-analyzer)
3. [Code](#code)
6. [Debugging](#debugging)
8. [Test/Results](#test/results)
9. [Competition](#competition)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)

 
### Objectives or Purpose 
### Lab 7
This lab is designed to assist you in learning the concepts associated with the input capture features for your MSP430. A single ultrasonic rangefinder is attached to a servo motor that can rotate. You will program your MSP430 to use the rangefinder to determine whether your mobile robot is approaching a wall in front or on one of its sides. The skills you will learn from this lab will come in handy in the future as you start interfacing multiple systems.

1) **Required functionality:** Use the Timer_A subsystem to light LEDs based on the presence of a wall. The presence of a wall on the left side of the robot should light LED1 on your Launchpad board. The presence of a wall next to the right side should light LED2 on your Launchpad board. A wall in front should light both LEDs. Demonstrate that the LEDs do not light until the sensor comes into close proximity with a wall.

2) **B Functionality:** You need to fully characterize the sensor for your robot. Create a table and graphical plot with at least three data points that shows the rangefinder pulse lengths for a variety of distances from a maze wall to the front/side of your robot. This table/graph should be generated for only one servo position. Use these values to determine how your sensor works so you can properly use it with the servo to solve the maze.

2) **A Functionality:** Control your servo position with your remote! Use remote buttons other than those you have used for the motors. Note: you still need to be able to get readings from the ultrasonic sensor after changing your servo position with the remote.

### Lab 8
During this lab, you will combine the previous laboratory assignments and program your robot to autonomously navigate through a maze.

You must write a program that autonomously navigates your robot through a maze (Figure 1) while meeting the following requirements:

1. Your robot must always start at the home position.
2. Your robot is considered successful only if it finds one of the three exits and moves partially out of the maze.
3. A large portion of your grade depends on which door you exit.
  1. Door 1 - Required Functionality
  2. Door 2 - B Functionality
  3. Door 3 - A Functionality
    1. **You cannot hit a wall!**
  4. Bonus!  Navigate from the A door back to the entrance using the same algorithm.
    1. **You cannot hit a wall!**
4. Your robot must solve the maze in less than three minutes.
5. Your robot will be stopped if it touches the wall more than twice.
6. Your robot must use the ultrasonic sensor to find its path through the maze.


### Prelab 7
**Part I - Understanding the Ultrasonic Sensor and Servo
------------------------------------------------------

**Read the datasheets and answer the following questions**


#### Ultrasonic Sensor 
1.  How fast does the signal pulse from the sensors travel?
	-   1 in for every 148 us


2.  If the distance away from the sensor is 1 in, how long does it take for the
    sensor pulse to return to the sensor?  1 cm?
	- pulse width/148=1 in, pulse width = 148 uS
	- pulse width/58=1 cm, pulse width = 58 uS


3.  What is the range and accuracy of the sensor?
	- Range: 2 cm – 500 cm
	- Accuracy: 0.3 cm


4.  What is the minimum recommended delay cycle (in ms) for using the sensor?  How does this compare to the "working frequency"?
	- Minimum delay cycle: 50 ms
	- The working frequency is 40 kHz therefore the delay is double the working period

#### Servo
1.  Fill out the following table identifying the pulse lengths needed for each servo position:

| Servo Position | Pulse Length (ms) | Pulse Length (counts) |
|:----------------:|:-----------------:|:---------------------:|
| Left           |         1          |       1000                |
| Middle         |         1.5        |       1500                |
| Right          |         2          |       2000                |
    

Part II - Using the Ultrasonic Sensor and Servo
-----------------------------------------------

1. Create psuedocode and/or flowchart showing how you will *setup* and *use* the ultrasonic sensor and servo.


![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/FLowchart_zpssjsatgfa.jpg)

Servo on P1.2

```
void checkLeft()
{
  ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
  ADC10CTL1 = INCH_2;                       // input A3
  ADC10AE0 |= BIT2;                         // P1.3 ADC Analog enable
  ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;                // Select SMCLK
  P1DIR |= BIT0;                            // Set P1.0 to output direction

    ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
    __bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit
    if (ADC10MEM < 0x1FF)
      P1OUT &= ~BIT0;                       // Clear P1.0 LED off
    else
      P1OUT |= BIT0;                        // Set P1.0 LED on
}
```
2. Create a schematic showing how you will setup the ultrasonic sensor and servo.


![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/UpdatedSchematic_zpsts0j0kmo.jpg)

3. Include any other information from this lab you think will be useful in creating your program.  Small snippets from datasheets such as the ultrasonic sensor timing may be good for your report.
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/ServoBoard_zpssysrjxwj.jpg)
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/ServoSchematic_zpsj2pbtzqt.jpg)
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/Sensor_zpsj3iaikpj.jpg)

### Prelab 8

1.  Print out your grading sheet.

2.  Consider your maze navigation strategy.  **Provide pseudocode and/or a flowchart** that shows what your main program loop will do.
    - This should include your collision avoidance algorithm.


```
while(1)
moveforward();
checkCenter();
if(DIST < 4){
	checkRight();
	if(DIST > 4){
		turnRight();
		moveForward();
	}
	else{
		checkLeft();
		if(DIST > 4){
			turnLeft();
			moveForward();
		}
else{
	moveforward();
}

```

3.  Include whatever other information from this lab you think will be useful in creating your program.

### Hardware
From the previous lab, the hardware was an MSP430, motor drive chip, IR sensor, voltage regulator and the robot motor.  The hardware added for this lab was the ultra sensor and the servo.


![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/IMG_5329_zpsu9eqy1rn.jpg)
######Figure 1: This is an image of Daaaave and Daaaave 2.0!

### Code
The required functionality was accomplished first by setting the proper pins for the GPIO as well as the trigger.  The interrupt was enabled on P1.4 and the trigger was set on P2.7.  I then focused on moving the servo in left, right and center.  I changed the TA0CCR1 to position the servo facing in a specific direction.  In order to face forward, the following code positions the servo.



```
	void turnCenter(){
	TA0CCR1 = 1725;
	TA0CTL |= MC_1;
	TA0CCTL1 |= OUTMOD_7;
	__delay_cycles(200000);
	TA0CCTL1 |= OUTMOD_0;
	TA0CTL &= ~MC_1;
}
```




After turning the servo, I worked on the ultra sensor to find the distance of an object in front of the sensor.  In order to do that, I sent a pulse from the ultra sensor and waited from an echo back.  When the echo returned back, the function enabled the interrupt.  When the interrupt enabled, the LED lights (based off of the direction of the servo) turned on.





```
void checkDistance(){
	TA0CCR0 = 50000;
	P2OUT |= BIT7;
	__delay_cycles(10);
	P2OUT &= ~BIT7;
	__delay_cycles(10000);
	TA0CCR0 = 20000;
}
```




The interrupt was enabled and the distance of the object was found by setting the timer value to zero and calculating the distance based on the echo pulsation. 
 
```
	if(P1IN & BIT4){
		TA0R = 0;
		TA0CTL |= MC_1;
		P1IES |= BIT4;
	}
	else{
		TA0CTL &= ~MC_1;
		DIST = (TA0R / 148) ;
//		DIST = (TA0R) ;				// B functionality
		P1IES &= ~BIT4;
	}
	P1IFG &= ~BIT4;
}
```

For lab 8, the only significant code is the ability to manipulate loops.  I created multiple loops to check where the wall was and to turn away from the wall.

		
### Debugging
There was a lot of debugging during while finding the required functionality.  I forgot to enable the interrupt and it caused a lengthy debugging period for me because I was unable to get my sensor to detect anything.  I also forgot to disable the flag interrupt at the end of the interrupt function.  These were the two major debugging for lab 7.  I tweaked a lot of the delay cycle times to affect how quickly an object was detected and how long the servo stayed in a certain position.

Most of the debugging for lab 8 was tweaking the delay cycles and TACCR0 values to ensure Daaaave made precise movements.  If he over rotated on a turn, he would run into the wall at an angle and not be able to move out of the maze.  Some of debugging also came from having to test my chips.  I wasn't receiving any output on both my MSP430 and my motor drive chip.  In the middle of my lab, the motors stopped working.  After testing the chips, I found that they we burnt out.

I had A LOT of debugging in lab 8.  It just so happened that with 2 burnt chips, my servo stopped working.  It stopped moving to the left.  I worked with a new robot and it was very difficult.  The location of the servo had to be adjusted as well as the TACCR values.  An interesting thing that happened was that my new robot's motor was backward.  My previous robot worked by having the GPIO on the motor reverse it's rotation.  When I ran my robot on "forward," it started to move backward!  I had to enable and disable the GPIO on the left and right to manipulate it to work with the new robot.  It took a while, but I got it!

### Test/Results
Required functionality was achieve for lab 7.

B Functionality is shown below.

![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/BFunctionality_zpsj4wtplnl.jpg)

My sensor is 163 us/in so it is a little slower than the datasheet claimed.  It is affecting my code by have Daaaave take a little longer to detect items.

I was unable to achieve functionality A for lab 7.  I attempted it but did not complete it by the due date.  I started with using a similar code to lab 6 to control the direction of the servo.  From there, I would have made sure the sensor still worked and detected objects at the same rate as before.

I achieved lab 8 required functionality: [https://youtu.be/oNP8SSX5DCI](https://youtu.be/oNP8SSX5DCI "Required Functionality")

I also achieved lab 8 B functionality.  The lab requirements for B functionality stated that "Your robot will be stopped if it touches the wall more than twice."  Daaaave only hit the wall once! [https://youtu.be/YLkPq8eyAJk](https://youtu.be/YLkPq8eyAJk "B Functionality")

I did achieve B functionality without hitting the wall with my old robot but could not achieve A functionality because my servo did not turn left.  I proudly achieved A functionality with my new robot, Daaaave 2.0: [https://www.youtube.com/watch?v=7DmWt8e8LVY](https://www.youtube.com/watch?v=7DmWt8e8LVY "A functionality") (please note the commentary!)

I'm even more proud of Daaaave 2.0 achieving the bonus!!!!!! [https://www.youtube.com/watch?v=XwsjHfZCiCM](https://www.youtube.com/watch?v=XwsjHfZCiCM "bonus")

### Competition
The videos of my B functionality and bonus functionality completed the maze at around 30 seconds.  I wanted to increase the speed Daaaave 2.0 (mainly because I was tired of waiting for him to finish).  During this process, I discovered that I could potentially overshoot the second turn to head straight for the door.  I made it through twice, however, I didn't feel confident with it.  I tweaked my function that rotates Daaaave 2.0 to the right.  Fortunately, I was able to get Daaaave 2.0 through the maze in the first time trial with an amazing time of 7.26 seconds!  Daaaave 2.0 was also able to leave the maze (after many collisions and time) place toward the back corner of the maze.

### Observation and Conclusions
The purpose of both labs was to create a robot to maneuver through a maze using ultrasonic sensors and the pulse-width-modulation features of the MSP430.  I was able to fulfill the purpose of the lab.  I completed every functionality for lab 8 but did not complete A functionality for lab 7.

I learned a lot from this dual lab.  I definitely learned a great amount about using the ultrasonic sensor.  It seemed complex at the beginning of the lab, but now that I have worked numerous hours on Daaaave, it's a function in my lab.  I have more experience with testing the chips if they are functioning properly (because I burnt out 2).

In the future, I can use my knowledge from this lab to do many things.  The different parts of the lab can be used for all types of projects.  It still blows my mind that everyone in this class created a robot.  It was very exciting to take our knowledge and everything we've learned in this class to build a robot.  In the beginning of the class, I did not think I would be able to build a robot.  I thought it was just a project that Capt Warner had on display in his officer.  It is very rewarding knowing that I was able to create a moving, seeing robot!  I finished on an amazing run with Daaaave 2.0.  It was the best feeling to end the semester with a competitive maze time!

### Documentation
C2C Mireles helped me with my code.  He helped explain to me how to set the interrupt and finding the distance of the echo pulse.  He explained how the pulse will go out and come back and the distance can be found using the prelab knowledge of 148 us/inch.  C2C Roseler helped explain to me finding a different method of calculate the distance of the object.  C2C Yi helped me find if my ultra sensor was working.  He helped me with the logic analyzer to show me the different pulse lengths on the trigger port.  C2C Hanson helped me debug my code.  He helped me change 3 lines of code where I cleared my interrupt flag on the timer as well as enabling the interrupt on P1.4.  He also helped me on the reaction time of my LED lights by changing the delay cycle times.  I also received help from Capt Warner to test my servo and trouble shoot my robot.  Capt Falkinburg helped me troubleshoot my robot and test if my motor driver chip was burnt out (which it was).