/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 1 Dec - 9 Dec 2016
Course: ECE382
File: lab8.h
HW: Lab8

Purp: Each function used to control the robot.

Doc: C2C Mireles helped me understand how changing the TACCR values affect the wheel speed and the set up for the required functionality.  C2C Hanson helped me debug my required functionality and A functionality.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef LAB8_H_
#define LAB8_H_

/*
 * Enable: Bit 0
 * PWM: Bit 1 and 4
 * GPIO: Bit 2 and 3
 */
void moveForward();
void forward();
void backward();
void left();
void right();
void slightLeft();
void slightRight();
void stop();

void checkDistance();
void init_MSP();
void turnCenter();
void turnRight();
void turnLeft();

void centerON();
void leftON();
void rightON();
unsigned int DIST;

//-----------------------------------------------------------------
// Page 76 : MSP430 Optimizing C/C++ Compiler v 4.3 User's Guide
//-----------------------------------------------------------------
typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0

//-----------------------------------------------------------------
// Function prototypes found in lab5.c
//-----------------------------------------------------------------
void initMSP430();
void delay65ms();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);


//-----------------------------------------------------------------
// Each PxIES bit selects the interrupt edge for the corresponding I/O pin.
//	Bit = 0: The PxIFGx flag is set with a low-to-high transition
//	Bit = 1: The PxIFGx flag is set with a high-to-low transition
//-----------------------------------------------------------------

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6

#endif /* LAB8_H_ */
