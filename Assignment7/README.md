# Assignment 7 - Pong

## By Sharee Acosta

### INITIAL VALUES
![](http://i.imgur.com/s1tHL1i.jpg) 
#### Figure 1: The screen shot shows the initial values of the ball (position, velocity and radius).

### RIGHT BOUNDARY
![](http://i.imgur.com/ONDBKw4.jpg)
![](http://i.imgur.com/O6X8dho.jpg)
![](http://i.imgur.com/mDRdSKz.jpg)
#### Figure 2: The three screen shots show the right boundary.  The first picture shows the velocity as a positive value and the position is less than the screen width and radius.  In the second picture, the ball becomes within range of the radius and screen width so the velocity becomes negative.  The last picture shows that the ball moved away from the left boundary.

### LEFT BOUNDARY
![](http://i.imgur.com/JVIgNfY.jpg)
![](http://i.imgur.com/3tj9EGb.jpg)
![](http://i.imgur.com/seNKIrH.jpg)
#### Figure 3: The three screen shots show the left boundary.  The first picture shows the velocity as a negative value and the position is greater than the left side of the screen.  In the second picture, the ball becomes within range of the radius and the left side of the screen and the velocity changes to a positive value.  The last picture shows that the ball moved away from the left boundary.

### BOTTOM BOUNDARY
![](http://i.imgur.com/crHspyH.jpg)
![](http://i.imgur.com/jznNmeg.jpg)
![](http://i.imgur.com/Zw77HKB.jpg)
#### Figure 4: The three screen shots show the bottom boundary.  The first picture shows the velocity as a positive value and the position is less than the screen height and radius.  In the second picture, the ball becomes within range of the radius and screen height so the velocity becomes negative.  The last picture shows that the ball moved away from the bottom boundary.

### TOP BOUNDARY
![](http://i.imgur.com/heB6Bea.jpg)
![](http://i.imgur.com/gkiOURq.jpg)
![](http://i.imgur.com/CfTKqE3.jpg)
#### Figure 5: The three screen shots show the bottom boundary.  The first picture shows the velocity as a negative value and the position is greater than the top of the screen.  In the second picture, the ball becomes within range of the radius and the top of the screen and the velocity changes to a positive value.  The last picture shows that the ball moved away from the top boundary.


Answers to the following questions in readme.md file:
	-How did you verify your code functions correctly?
		I verified my code functions correctly by first ensuring it had the values I set for it.  Then I use hand calculations to ensure the all was moving in the correct direction according to the given position and velocity.  Then I calculated the boundary that the ball could not pass and ensured it did not pass that value during the simulation.  The simulation matched my hand calculations.
	-How could you make the "collision detection" helper functions only visible to your implementation file (i.e. your main.c could not call those functions directly)?
		You could place the collision detection in seperate file that is called by the implementation file.

### Documentation
C2C Hanson helped me debug my code and C2C Hayden helped me understand the computation for the Fibonacci number.C2C Hayden explained how to create the functions for the boundary conditions.  C2C Lang helped me debug and explained the C language to create the functions.  C2C Talasaga helped me debug my boundary simulation.
