/*
 * Implementation.c
 *
 *  Created on: Oct 11, 2016
 *      Author: C18Sharee.Acosta
 */
#include "Header.h"

/*------------------------------------------------------------------------/
* createBall
* Sharee Acosta
* Create a ball to with specific coordinates
* Inputs
* 	xPos, yPos, xVel, yVel, radius
* Outputs
* 	ball.position.x
* 	ball.position.y
* 	ball.velocity.x
* 	ball.velocity.y
* 	ball.radius
*/
ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius)		// define the ball
{

	ball_t ball;
	ball.position.x = xPos;
	ball.position.y = yPos;
	ball.velocity.x = xVel;
	ball.velocity.y = yVel;
	ball.radius = radius;

	return ball;
}

/*------------------------------------------------------------------------/
* moveBall
* Sharee Acosta
* Move the ball with the changing position based on the velocity with bounds
* Inputs
* 	ball.position.x
* 	ball position.y
* 	ball.velocity.x
* 	ball.velocity.y
* Outputs
* 	ball.position.x
* 	ball position.y
* 	ball.velocity.x
* 	ball.velocity.y
*/
ball_t moveBall(ball_t ballToMove)			// move the ball according to the position and velocity with boundary considerations
{
	ball_t ball;
	ball.position.x += ball.velocity.x;
	ball.position.y += ball.velocity.y;


	if (RightBound(ball) == TRUE)
	{
		ball.velocity.x = -ball.velocity.x;
	}
	if (LeftBound(ball) == TRUE)
	{
		ball.velocity.x = -ball.velocity.x;
	}
	if (TopBound(ball) == TRUE)
	{
		ball.velocity.y = -ball.velocity.y;
	}
	if (BottomBound(ball) == TRUE)
	{
		ball.velocity.y = -ball.velocity.y;
	}
	return ball;
}

unsigned char LeftBound(ball_t ball)
{
	if (ball.position.x - ball.radius < abs(ball.velocity.x))
			{
				return TRUE;
			}
	else		return FALSE;
}

unsigned char RightBound(ball_t ball)
{
	if (ball.position.x  + ball.radius >= SCREEN_WIDTH)
			{
				return TRUE;
			}
	else		return FALSE;
}

unsigned char TopBound(ball_t ball)
{
	if (ball.position.y - ball.radius <= 0)
			{
				return TRUE;
			}
	else		return FALSE;
}

unsigned char BottomBound(ball_t ball)
{
	if (ball.position.y + ball.radius >= SCREEN_HEIGHT)
			{
				return TRUE;
			}
	else		return FALSE;
}
