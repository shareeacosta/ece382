/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 10 October 2016
Course: ECE 382
File: pong.h
Event: Assignment 7 - Pong

Purp: Implements a subset of the pong game

Doc:    C2C Hayden explained how to create the functions for the boundary conditions.  C2C Lang helped me debug and explained the C language to create the functions.  C2C Talasaga helped me debug my boundary simulation.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h>
#include "Header.h"

/*
 * main.c
 */
	void main(void) {
    WDTCTL = WDTPW | WDTHOLD;					// Stop watchdog timer

    ball_t ball = createBall(55,33,88,22,50);	// create the ball with specific values

	while(1) {		// CUP TRAP
		ball = moveBall(ball);					// move and place the new ball position and velocity
	}
}
