/*
 * Header.h
 *
 *  Created on: Oct 11, 2016
 *      Author: C18Sharee.Acosta
 */

#ifndef _PONG_H
#define _PONG_H

#define SCREEN_WIDTH 250
#define SCREEN_HEIGHT 250

#define TRUE 1
#define FALSE 0

typedef struct {
    int x;
    int y;
} vector2d_t;

typedef struct {
    vector2d_t position;
    vector2d_t velocity;
    unsigned char radius;
} ball_t;


ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius);

ball_t moveBall(ball_t ballToMove);


#endif
 /* HEADER_H_ */
