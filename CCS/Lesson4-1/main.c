            ; first thing's first - how do we create a comment?

            mov.w   #0x0200, r5
            mov.w   #0xbeef, r6

fill        mov.w   r6, 0(r5)           ; anyone know what this syntax means?
            incd    r5
            cmp.w   #0x0400, r5         ; what does this instruction do?
            jne     fill

forever     jmp     forever
