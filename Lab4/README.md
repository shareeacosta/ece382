[https://bitbucket.org/Talosaga_Anthony/ece382_talosaga_acosta/src](https://bitbucket.org/Talosaga_Anthony/ece382_talosaga_acosta/src)

#Lab 4 - C - "Etch-a-Sketch and Pong"

## By Sharee Acosta and Anthony Talosaga

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Prelab](#prelab)
2. [Hardware](#hardware)
3. [Logic Analyzer](#logic-analyzer)
3. [Code](#code)
6. [Debugging](#debugging)
8. [Test/Results](#test/results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)

 
### Objectives or Purpose 
In this lab, we used C to create an etch-a-sketch-type program that utilizes some subroutines from Lab 3.  We were expected to write clean, maintainable, modular code that is committed regularly to Git.

1) **Required functionality:** Create a block that is 10x10 box to draw and erase like an etch-a-sketch.

2) **B Functionality:** Have a box bounce off the screen walls.

3) **A Functionality:** Incorporate functionality B and create a ping pong game.

4) **Bonus Functionality:** Change the speed on the box during the ping pong game.


### Prelab
The prelab consisted of understanding data types, calling/return convention and cross language build constructs.

| Size | Signed/Unsigned | Type | Min Value | Max Value |
|:-:|:-:|:-:|:-:|:-:|
| 8-bit | Unsigned | Unsigned char, bool, _Bool | 0 | 225 |
| 8-bit | Signed | Signed char | -128 | 127 |
| 16-bit | Unsigned | Unsigned Short | 0 | 65,535 |
| 16-bit | Signed | Short, signed short int, signed int | -32,768 | 32,767 |
| 32-bit | Unsigned | Unsigned Long | 0 | 4,294,967,295 |
| 32-bit | Signed | Long, signed long | -2,147,483,648 | 2,147,483,647 |
| 64-bit | Unsigned | Unsigned long long | 0 | 18,446,744,073,709,551,615 |
| 64-bit | Signed | Long long, signed long long | -9,223,372,036,854,775,808 | 9,223,372,036,854,775,807 |


##### Table 1: The table shows the understanding of each signed and unsigned value sized bit.  It shows the data type that produces a variable of the given size.  It also shows the minimum and maximum values of the data type.

| Type | Meaning | C typedef declaration |
|:-:|:-:|:-:|
| int8 | unsigned 8-bit value | typedef unsigned char int8; |
| sint8 | signed 8-bit value | typedef char sint8; |
| int16 | unsigned 16-bit value | typedef unsigned short int16; |
| sint16 | signed 16-bit value | typedef short sint16; |
| int32 | unsigned 32-bit value | typedef unsigned long int32; |
| sint32 | signed 32-bit value | typedef long sint32; |
| int64 | unsigned 64-bit value | typedef unsigned long long int64; |
| sint64 | signed 64-bit value | typedef long long sint64; |


##### Table 2: The table shows the understanding of data representation.  It shows the meaning of the data type and what it is declared as in C.  Variables can be different length sizes and they are declared with the data types above.

| Iteration | a | b | c | d | e |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 1st | 2 | 2 | 3 | 4 | 2 |
| 2nd | 8 | 9 | 8 | 7 | 8 |
| 3rd | 14 | 15 | 15 | 13 | 15 |
| 4th | 20 | 21 | 20 | 19 | 20 |
| 5th | 26 | 27 | 26 | 25 | 26 |


##### Table 3: The table shows the understanding of program "simpleLab4.c."  It shows the values during each iteration of the program.

| Parameter | Value Sought |
|:-:|:-:|
| Starting address of func | 0xcoba |
| Ending address of func | 0xc0c6 |
| Register holding w | R12 |
| Register holding x | R13 |
| Register holding y | R14 |
| Register holding z | R15 |
| Register holding return value | R12 |


##### Table 3: The table shows the understanding of the disassembly of the program "simpleLab4.c."  It shows which register are used to to return the value from function to main.

The last part of the prelab was cross language build constructs.  There were questions about the extern and global directives.

######What is the role of the extern directive in a .c file? Hint: check out the external variable Wikipedia page. 
- The role of an extern directive is to declare a variable without defining it by assigning an initialization value to a variable.  It allows external files to reference functions defined in them by the complied program.  However, the complier complies the code without "seeing" the other files.
######What is the role of the .global directive in an .asm file (used in lines 60-64)? Hint: reference section 2.6.2 in the MSP 430 Assembly Language Tools v4.3 User's Guide. 
- The role of a global directive can either be defined or referenced.  The assembler chooses the appropriate directive.  The def directive is defined in the current file and can be used in another file.  The ref directive is referenced in the current file but can be defined in another file.  It allows functions to be reference by external files when the linker links 2 or more object files together.

### Hardware
The hardware for this lab was the MSP430 and the LED BoosterPack.

### Code
For the required functionality, the program created an "etch-a-sketch" where the LCD boosterpack controlled the position of the paint brush.  The paint brush drew 10x10 blocks of pixels in a given direction.  The up, down, right and left direction were controlled by the S4, S3, S2 and S5 buttons respectively.  When the S1 button was pressed, it changed the paint brush to an eraser that worked the same way.  The S1 button could change the function between paint brush and eraser as many times as the user pleased.  The paint brush/eraser is also robust to not draw off the screen but remain in the same place if directed to go out of the boundaries.  The code shows an examples of the code of the up button (S4) in both paint and eraser versions.  Mode 2 erases and mode 1 paints green pixels.
	
	while(condition2) {
		if (mode == 2) {
			if (UP_BUTTON == 0) {
				Delay160ms();
				if (hitTop(y, 10) == FALSE) {
					drawBox(x, y, CLEAR);
					y -= velocity;
					drawBox(x, y, WHITE);
				}
			}

		if (mode == 1) {
			drawBox(x, y, GREEN);

			if (UP_BUTTON == 0) {
				Delay160ms();
				if (hitTop(y, 10) == FALSE) {
					y = y - velocity;
					drawBox(x, y, GREEN);
				}
			}

For the B functionality, the 10x10 box bounces off the boundaries of the screen.  It uses the program of assignment 7 by changing the velocity direction when it hits any screen boundary.  The code below shows the main code to move the box and change it velocity.

		ball moveBall(ball myBall, int width_limit, int height_limit) {
		myBall.position.x += myBall.velocity.x;		//First, it increments/decrements the x- and y- position of myBall
		myBall.position.y += myBall.velocity.y;

		if (hitLeft(myBall.position.x, myBall.radius)) {	//Invert velocity.x when hit left edge
			myBall.velocity.x = -myBall.velocity.x;
		}

		if (hitRight(myBall.position.x, myBall.radius, width_limit)) {		//Invert velocity.x when hit right edge
			myBall.velocity.x = -myBall.velocity.x;
		}

		if (hitTop(myBall.position.y, myBall.radius)) {		//Invert velocity.y when hit top edge
			myBall.velocity.y = -myBall.velocity.y;
		}

		if (hitBottom(myBall.position.y, myBall.radius, height_limit)) {	//Invert velocity.y when hit bottom edge
			myBall.velocity.y = -myBall.velocity.y;
		}

		return myBall;

For the A functionality, functionality B was improved to turn into a ping pong game.  A single paddle was created on the bottom of the screen for the 10x10 box to bounce off of.  The game would function regularly until the box missed the paddle.  The paddle was controlled by the left and right buttons (S2 and S5 respectively).  The code below shows the function of the paddle to move the paddle left and right.

		drawBox(myBall.position.x, myBall.position.y, GREEN);
		drawPaddle(myPaddle.position.x, myPaddle.position.y, PINK);

		if (MAIN_BUTTON == 0) {
			Delay160ms();
			Delay160ms();
			Delay160ms();
			Delay160ms();
			pause = FALSE;
		}

		if (LEFT_BUTTON == 0) {
			if (hitLeft(myPaddle.position.x, 40) == FALSE) {
				drawPaddle(myPaddle.position.x, myPaddle.position.y, CLEAR);
				myPaddle.position.x -= 5;
				drawPaddle(myPaddle.position.x, myPaddle.position.y, PINK);
			}

		}
		if (RIGHT_BUTTON == 0) {
			if (hitRight(myPaddle.position.x, 40, SCREEN_WIDTH) == FALSE) {
				drawPaddle(myPaddle.position.x, myPaddle.position.y, CLEAR);
				myPaddle.position.x += 5;
				drawPaddle(myPaddle.position.x, myPaddle.position.y, PINK);
			}
		}

There were many different parts to the bonus.  During functionality B and A, it was made more robust by hitting S4 to speed up the box or S3 to slow down the box (which essentially meant you could make the game harder or easier depending on your skill level of ping pong and knowledge of geometry angles).  The box could also be paused by pressing S1 during those two functionalities.

		if (pause == FALSE) {
			drawBox(myBall.position.x, myBall.position.y, CLEAR);

			if (myBall.position.y + 7 >= myPaddle.position.y) {
				if (myBall.position.x + 2 > myPaddle.position.x - 40 && myBall.position.x - 2 < myPaddle.position.x + 40) {
					myBall.velocity.y = -myBall.velocity.y;
					myBall = moveBall(myBall, SCREEN_WIDTH, SCREEN_HEIGHT);
				}
				else {
					condition = FALSE;
				}
			}
			else {
				myBall = moveBall(myBall, SCREEN_WIDTH-2, SCREEN_HEIGHT);
			}

		if (UP_BUTTON == 0) {
			if (myBall.velocity.x < 0) {myBall.velocity.x -= 1;}
			else {myBall.velocity.x += 1;}
			if (myBall.velocity.y < 0) {myBall.velocity.y -= 1;}
			else {myBall.velocity.y += 1;}
			Delay160ms();
			Delay160ms();
		}
		if (DOWN_BUTTON == 0) {
			if (myBall.velocity.x < 0) {myBall.velocity.x += 1;}
			else {myBall.velocity.x -= 1;}
			if (myBall.velocity.y < 0) {myBall.velocity.y += 1;}
			else {myBall.velocity.y -= 1;}
			Delay160ms();
			Delay160ms();
		}

Another cool part of the program is that when it is first started, the up, down and right button can determine which functionality to play with!  The up buttons goes to ping pong, the right button goes to a bouncing box and the down button goes to the etch-a-sketch.  This bonus feature also occurs when the etch-a-sketch is on and it is on mode 3 (pink box).  The user needs to confirm its mode and can choose the next functionality!

	if (mode) {
		if (MAIN_BUTTON == 0) {mode = 0;}
		if (UP_BUTTON == 0) { //game1_reset
			clearScreen();
			game = 1;

		} //game1_reset_end

		if (DOWN_BUTTON == 0) { //game2_reset
			clearScreen();
			game = 2;
		} //game2_reset_end

		if (RIGHT_BUTTON == 0) { //game3_reset
				clearScreen();
				game = 3;
		} //game3_reset_end
	}


		if (mode == 3) {
			drawBox(x, y, PINK);

			if (UP_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				condition2 = FALSE;
			}

### Debugging
Our functionalities had a multiple problems.  On our required function did not have a 10x10 box and the box ran off the screen.  In order to fix that, we decremented the width of the box because it was a rectangle.  In order to fix the boundaries of the box, we had to create the functions hitTop, hit Bottom, hitLeft and hitRight to check if the position of the box was within the screen.  If the the "radius" of the box was not within the screen limits, the box would stay in the same place.

The B functionality had a problem of disappearing on transition.  The box was not being drawn fast enough and in order to fix that program, we moved the delay function.  It was being incorporated into too many loops which made the box appear as a ghost.  The box also was not within the screen limits, however, it was fixed when the debugging in the required functionality was completed.  But a main focus for fixing this problem was ensuring that the radius did not exceed the screen limits.

In the A functionality, the entire game would slow down whenever the paddle was moved.  This was caused to the delay when the left and right buttons were pressed.  The B functionality and the paddle functions were in the same while loop.  The delays were in the paddle functions which caused the entire game to slow down.  In order to fix that, one delay was placed in the beginning of the while loop.

### Test/Results
The results of the each functionality was successful.  After debugging, the program ran perfectly with extra perks.

##### Required Functionality
The video link shows the required functionality of an etch-a-sketch.  In one mode, it paints green 10x10 pixels and in the other mode it erases the paint.  The modes are changed between the S1 button.  It also shows that the paint brush/eraser does not exceed the boundaries of the screen.
[https://youtu.be/xnMIFKvhS1M](https://youtu.be/xnMIFKvhS1M)

##### B Functionality
The video link shows the B functionality of a box bouncing off the walls.  It changes its direction every time it hits a screen wall.
[https://youtu.be/pUjwbBYAf-o](https://youtu.be/pUjwbBYAf-o)

##### A Functionality
The video link shows the A functionality of the ping pong game.  The paddle moves and the box bounces off of it.  When the paddle does not catch the box, the game ends.
[https://youtu.be/Aerg4nbVtFk](https://youtu.be/Aerg4nbVtFk)

##### Bonus
The video link shows the bonus function of changing the speed of the box and the ability to pause the game and still move the paddle!  It also shows the bonus of choosing the ping pong function by pressing the S4 button.
[https://youtu.be/mPDMuvA4Lao](https://youtu.be/mPDMuvA4Lao)

The video link is a little long but it shows the ability to change to different functions from the etch-a-sketch when it is on mode 3 (pink box).  When it is on mode 3, the user needs to confirm the mode by pressing the S4 (up) button.  It also shows the ability to choose the functionality when it is restarted.
[https://youtu.be/7c6XAo7mpyU](https://youtu.be/7c6XAo7mpyU)

### Observation and Conclusions
The purpose of the lab was to use C to create an etch-a-sketch-type program that utilizes some subroutines from Lab 3.  This program has 3 different functionalities.  The program creates a 10x10 box which moves across the screen like a paint brush using the S2, S3, S4 and S5 buttons.  It also can be changed to an eraser that uses the same direction movement as the paint brush.  The next functionality has the 10x10 box bounce off the screen walls by changing the velocity direction.  The last functionality incorporates the previous functionality to play a game of ping pong.  The paddle is controlled by the left and right buttons and the game ends when the paddle does not block the box from the bottom screen limit.  We were able to fulfill the purpose of the lab with more than the requirements! 

After completing this lab, we learned a lot.  Individually, Sharee learned more coding practices.  Because she is still learning C, it was a good exposure on converting the assembly to C.  Tony learned to improve his C language as well as his ability to debug.  Overall, we both learned how to work together and delegate different parts of the lab.  We bounced ideas back and forth on how to accomplish the lab.  However, we did not learn to properly share the repo.  Sharee has access and administer rights but can't push anything to the shared repo.

The video shows that we accomplished the 3 functionalities of the lab as well as bonus points.

In the future, we can use this coding features to act like screen savers.  We could have program wait a certain amount of time for a button to be pressed and when it has been inactive for that time, it could draw a picture.  Tony created an image by increasing the speed of the box on functionality A essentially where the box draws while it moves.  It created a cool patterns that could be incorporated into a screen saver where it draws over and over and changes colors after hitting a wall so many times.  It would be a pretty cool bonus for future labs!

### Documentation
C2C Yi gave us ideas on how to get the bonus points.  C2C Hanson helped explain some of the concepts of the prelab.

