;------------------------------------------------------------------------------
; Lab 1 - Simple Calculator
; C2C Sharee Acosta
; ECE382  /T4
; 6 Sep 2016 - 8 Sep 2016
;
; This program works as a calculator.  It reads an array and adds, subtracts, clears or ends the "calculator."
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have

                                            ; references to current section.
myProgram:		.byte		0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55	; defined in ROM
ADD_OP:			.equ		0x11
SUB_OP:			.equ		0x22
CLR_OP:			.equ		0x44
END_OP:			.equ		0x55

				.data						; assemble into RAM memory
myResults:		.space		20				; reserving 20 bytes of space

				.text						; assemble into program memory
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

				mov			#myProgram, R4		; read program from memory (ROM)
				mov.b		#0x00, R11			; set value for clear op
				mov			#myResults, r10		; write results to memory (RAM)

restart:		mov.b		@R4+, R5			; read first operand and increment array

loop:
				mov.b		@R4+, R6			; read operator and increment array
				cmp.b		#ADD_OP, R6			; compare if it is an add op 0x11
				jeq			add					; go to add loop if it op
				cmp.b		#SUB_OP, R6			; compare if it is an sub op 0x22
				jeq			sub					; go to subtract loop if it is op
				cmp.b		#CLR_OP, R6			; compare if it is an clear op 0x44
				jeq			clr					; go to clear loop if it is op
				jmp			end					; if none, go straight to end op

add:

				mov.b		@R4+, R7			; take second operand
				add.b		R5, R7				; add it
				mov.b		R7, 0(R10)			; place in results
				mov.b		@R10+, R5			; increment
				jmp		 	loop				; restart loop and grab next operand

sub:
				mov.b		@R4+, R7			; take second operand
				sub.b		R7, R5				; subtract it
				mov.b		R5, 0(R10)			; place in result
				mov.b		@R10+, R5			; increment
				jmp			loop				; restart loop and grab next operand

clr:
				mov.b		@R10+, R5			; increment
				mov.b		R11, 0(R10)			; place 0x00 in results
				jmp			restart				; go and read a new operand

end:
				jmp			forever				; send straight to CUP trap

forever:		jmp			forever				; CUP trap
                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
