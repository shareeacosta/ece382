/*
 * lab7.h
 *
 *  Created on: Nov 29, 2016
 *      Author: C18Sharee.Acosta
 */

#ifndef LAB7_H_
#define LAB7_H_

void checkLeft();
void checkCenter();
void checkRight();
void checkDistance();
void init_MSP();
unsigned int DIST;

//#define		redON		P1OUT |= BIT0
//#define		redOFF		P1OUT &= ~BIT0
//#define		greenON		P1OUT |= BIT6
//#define		greenOFF	P1OUT &= ~BIT6

//-----------------------------------------------------------------
// Page 76 : MSP430 Optimizing C/C++ Compiler v 4.3 User's Guide
//-----------------------------------------------------------------
typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0

//-----------------------------------------------------------------
// Function prototypes found in lab5.c
//-----------------------------------------------------------------
void initMSP430();
void delay65ms();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);


//-----------------------------------------------------------------
// Each PxIES bit selects the interrupt edge for the corresponding I/O pin.
//	Bit = 0: The PxIFGx flag is set with a low-to-high transition
//	Bit = 1: The PxIFGx flag is set with a high-to-low transition
//-----------------------------------------------------------------

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6


#endif /* LAB7_H_ */
