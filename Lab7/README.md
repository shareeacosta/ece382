# Lab 7 and 8 - A/D Conversion and Robot Maze - "Robot Sensing"

## By: Sharee Acosta

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Prelab 7](#prelab-7)
3. [Prelab 8](#prelab-8)
2. [Hardware](#hardware)
3. [Logic Analyzer](#logic-analyzer)
3. [Code](#code)
6. [Debugging](#debugging)
8. [Test/Results](#test/results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)

 
### Objectives or Purpose 
This lab is designed to assist you in learning the concepts associated with the input capture features for your MSP430. A single ultrasonic rangefinder is attached to a servo motor that can rotate. You will program your MSP430 to use the rangefinder to determine whether your mobile robot is approaching a wall in front or on one of its sides. The skills you will learn from this lab will come in handy in the future as you start interfacing multiple systems.

1) **Required functionality:** Use the Timer_A subsystem to light LEDs based on the presence of a wall. The presence of a wall on the left side of the robot should light LED1 on your Launchpad board. The presence of a wall next to the right side should light LED2 on your Launchpad board. A wall in front should light both LEDs. Demonstrate that the LEDs do not light until the sensor comes into close proximity with a wall.

2) **B Functionality:** You need to fully characterize the sensor for your robot. Create a table and graphical plot with at least three data points that shows the rangefinder pulse lengths for a variety of distances from a maze wall to the front/side of your robot. This table/graph should be generated for only one servo position. Use these values to determine how your sensor works so you can properly use it with the servo to solve the maze.

2) **A Functionality:** Control your servo position with your remote! Use remote buttons other than those you have used for the motors. Note: you still need to be able to get readings from the ultrasonic sensor after changing your servo position with the remote.


### Prelab 7
**Part I - Understanding the Ultrasonic Sensor and Servo
------------------------------------------------------

**Read the datasheets and answer the following questions**


#### Ultrasonic Sensor 
1.  How fast does the signal pulse from the sensors travel?
	-   1 in for every 148 us


2.  If the distance away from the sensor is 1 in, how long does it take for the
    sensor pulse to return to the sensor?  1 cm?
	- pulse width/148=1 in, pulse width = 148 uS
	- pulse width/58=1 cm, pulse width = 58 uS


3.  What is the range and accuracy of the sensor?
	- Range: 2 cm – 500 cm
	- Accuracy: 0.3 cm


4.  What is the minimum recommended delay cycle (in ms) for using the sensor?  How does this compare to the "working frequency"?
	- Minimum delay cycle: 50 ms
	- The working frequency is 40 kHz therefore the delay is double the working period

    **Going further (optional):**
    Given the max "working frequency", does the maximum sensor range
    make sense?  <u>Hint</u>:  find the maximum unambiguous range.


#### Servo
1.  Fill out the following table identifying the pulse lengths needed for each servo position:

| Servo Position | Pulse Length (ms) | Pulse Length (counts) |
|:----------------:|:-----------------:|:---------------------:|
| Left           |         1          |       1000                |
| Middle         |         1.5        |       1500                |
| Right          |         2          |       2000                |
    

Part II - Using the Ultrasonic Sensor and Servo
-----------------------------------------------

1. Create psuedocode and/or flowchart showing how you will *setup* and *use* the ultrasonic sensor and servo.
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/FLowchart_zpssjsatgfa.jpg)

Servo on P1.2

```
void checkLeft()
{
  ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
  ADC10CTL1 = INCH_2;                       // input A3
  ADC10AE0 |= BIT2;                         // P1.3 ADC Analog enable
  ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;                // Select SMCLK
  P1DIR |= BIT0;                            // Set P1.0 to output direction

    ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
    __bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit
    if (ADC10MEM < 0x1FF)
      P1OUT &= ~BIT0;                       // Clear P1.0 LED off
    else
      P1OUT |= BIT0;                        // Set P1.0 LED on
}
```
2. Create a schematic showing how you will setup the ultrasonic sensor and servo.
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/UpdatedSchematic_zpsts0j0kmo.jpg)

3. Include any other information from this lab you think will be useful in creating your program.  Small snippets from datasheets such as the ultrasonic sensor timing may be good for your report.
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/ServoBoard_zpssysrjxwj.jpg)
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/ServoSchematic_zpsj2pbtzqt.jpg)
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/Sensor_zpsj3iaikpj.jpg)

**Below are some things you should think about as you design your interfaces:**

 - **Consider if/how you will configure the input capture subsystem** for your ultrasonic sensor.  What are the registers you will need to use?  Which bits in those registers are important?  What is the initialization sequence you will need?  Should you put a limit on how long to sense?  If so, how long makes sense given the limitations of the sensor (or the requirements of the project)?

 - **Consider the hardware interface.**  Which signals will you use?  Which pins correspond to those signals?  How will you send a particular pulse width to the servo?

 - **Consider the software interface you will create to your sensor.**  Will you block or use interrupts?  Will you stop moving while you sense?

 - Will the ultrasonic sensor be ready to sense immediately after the servo changes position?  How do you know? 

 - How long should you keep sending PWM pulses?  Keep in mind that you may have to send many PWM pulses before your servo is in the correct position.  Even after that, can you stop sending PWM pulses?
 
 - **Consider how to make your code extensible.**  It will be easier to achieve the bonus functionality of creating an ultrasonic library if you design it right from the beginning.  You should also consider how you will make the sensor and servo work together.


### Hardware


### Code

		
### Debugging


### Test/Results


### Observation and Conclusions


### Documentation
C2C Yi helped explain to me how the function for turning left and right worked as well as reversing the robot.  C2C Mireles helped explain how the PWM worked.  He also helped me understand how changing the TACCR values affect the wheel speed and the set up for the required functionality.  C2C Hanson helped me debug my required functionality and A functionality.

