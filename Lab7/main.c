#include <msp430.h>
#include "lab7.h"

void main(void){
	init_MSP();
		while(1){
			checkLeft();
			__delay_cycles(750000);
			checkCenter();
			__delay_cycles(750000);
			checkRight();
			__delay_cycles(750000);
			checkCenter();
			__delay_cycles(750000);
	}
}

#pragma vector = PORT1_VECTOR
__interrupt void getDistance(void){
	if(P1IN & BIT4){
		TA0R = 0;
		TA0CTL |= MC_1;
		P1IES |= BIT4;
	}
	else{
		TA0CTL &= ~MC_1;
		DIST = (TA0R / 148) ;
//		DIST = (TA0R) ;				// B functionality
		P1IES &= ~BIT4;
	}
	P1IFG &= ~BIT4;
}

