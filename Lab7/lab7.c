/*
 * lab7.c
 *
 *  Created on: Nov 29, 2016
 *      Author: C18Sharee.Acosta
 */

#include <msp430g2553.h>
#include "lab7.h"

void init_MSP(){
	WDTCTL = WDTPW|WDTHOLD;

	P1DIR = BIT0|BIT6;				// Set the LED as output
	P1SEL &= ~(BIT0|BIT6);
	P1SEL2 &= ~(BIT0|BIT6);
	P1OUT &= ~(BIT0|BIT6);

	P1DIR |= BIT2;
	P1SEL |= BIT2;
	P1SEL2 &= ~BIT2;
	P1OUT &= ~BIT2;

	TA0CTL |= TASSEL_2|ID_0;		//MC_1
	TA0CTL &= ~TAIE;
	TA0CTL &= ~TAIFG;

	// GPIO input P1.4
	P1DIR &= ~BIT4;
	P1SEL &= ~BIT4;

	// enable interrupt on rising edge P1.4 and clear the flag.
	P1IFG &= ~BIT4;
	P1IES &= ~BIT4;
	P1IE |= BIT4;

	// trigger out 2.7
	P2DIR |= BIT7;
	P2SEL &= ~BIT7;
	P2SEL2 &= ~BIT7;
	P2OUT &= ~BIT7;

	TA0CCTL1 |= OUTMOD_7;
	TA0CCR0 = 20000;						// want 20 ms roll over for servo


	_enable_interrupt();
	return;
}

void turnLeft(){
	TA0CCR1 = 2800;
	TA0CTL |= MC_1;
	TA0CCTL1 |= OUTMOD_7;
	__delay_cycles(250000);
	TA0CCTL1 |= OUTMOD_0;
	TA0CTL &= ~MC_1;
}

void turnCenter(){
	TA0CCR1 = 1725;
	TA0CTL |= MC_1;
	TA0CCTL1 |= OUTMOD_7;
	__delay_cycles(200000);
	TA0CCTL1 |= OUTMOD_0;
	TA0CTL &= ~MC_1;
}

void turnRight(){
	TA0CCR1 = 650;
	TA0CTL |= MC_1;
	TA0CCTL1 |= OUTMOD_7;
	__delay_cycles(250000);
	TA0CCTL1 |= OUTMOD_0;
	TA0CTL &= ~MC_1;
}

void checkLeft(){
	turnLeft();
	checkDistance();
	if(DIST < 6){
	P1OUT |= BIT0;
	__delay_cycles(1500000);
	}
	P1OUT &= ~BIT0;
}

void checkCenter(){
	turnCenter();
	checkDistance();
	if(DIST < 6){
	P1OUT |= (BIT0|BIT6);
	__delay_cycles(1500000);
	}
	P1OUT &= ~(BIT0|BIT6);
}

void checkRight(){
	turnRight();
	checkDistance();
	if(DIST < 6){
	P1OUT |= BIT6;
	__delay_cycles(1500000);
	}
	P1OUT &= ~BIT6;
}

void checkDistance(){
	TA0CCR0 = 50000;
	P2OUT |= BIT7;
	__delay_cycles(10);
	P2OUT &= ~BIT7;
	__delay_cycles(10000);
	TA0CCR0 = 20000;
}



