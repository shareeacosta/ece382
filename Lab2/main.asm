;-------------------------------------------------------------------------------
; Sharee Acosta
; 13 Sep - 20 Sep 2016
; Capt Warner/T4
;
; This program decrypts an encrypted message using the XOR function.
;
; Documentation:  C2C Yi helped me understand how to push and pop the information.  C2C Orikogbo helped me get the B functionality and debug the problem of changing my "mov" to "mov.b"
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section

; Store Key Here
myKey:
		; Required Functionality
		.byte	0xac
		; B Functionality
		;.byte	0xac, 0xdf, 0x23
		; own test
		;.byte	0x03, 0x55, 0x5A, 0xFF
myMessage:
		; Required Functionality
		.byte	0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f
		; B Functionality
		;.byte	0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50
		; own test
		;.byte	0x4b,0x3c,0x76,0xdf,0x6e,0x2c,0x7a,0x91,0x62,0x38,0x3f,0xdf,0x6a,0x26,0x7a,0xac,0x6b,0x34,0x28,0x9a,0x66,0x75,0x1b,0x9c,0x6c,0x26,0x2e,0x9e,0x22,0x74
	.data
myResult:
		.space	100
	.text
numberBytes:		.equ		0x5E
		; required 0x5E
		; B 0x51
		; own 0x1E

numberKey:			.equ		0x01
		; required 0x01
		; B 0x03
		; own 0x04

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
                                            ; Main loop here
;-------------------------------------------------------------------------------

			mov.w		#myKey, R5
			mov.w		#myMessage, R6
			mov.w		#myResult, R7
			mov.b		#numberBytes, R8
			mov.b		@R6+, R10			; increment myMessage
			mov.b		#numberKey, R11
            call    	#decryptMessage

forever:    jmp     forever
;-------------------------------------------------------------------------------
                                            ; Subroutines
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;Subroutine Name: decryptMessage
;Author:
;Function: Decrypts a string of bytes and stores the result in memory.  Accepts
;           the address of the encrypted message, address of the key, and address
;           of the decrypted message (pass-by-reference).  Accepts the length of
;           the message by value.  Uses the decryptCharacter subroutine to decrypt
;           each byte of the message.  Stores theresults to the decrypted message
;           location.
;Inputs:
;Outputs:
;Registers destroyed:
;-------------------------------------------------------------------------------

decryptMessage:
		push		R10				; encrypted byte
		push		R8				; number of bytes
		push		R5				; key

		mov			R5, R12			; place key into new register to restart the key
counteradmin:
		call		#decryptCharacter
		mov.b		R10, 0(R7)		; place the encrypted byte in new register
		mov.b		@R6+, R10		; increment myMessage
		inc			R7				; increment myResult
		inc			R5				; increment	myKey
		dec			R8				; decrement numberBytes
		dec			R11				; decrement numberKey
		cmp.b		#0x00, R8		; see if there are more bytes in myMessage
		jeq			done
		jmp			numberofkeys

numberofkeys:
		cmp.b		#0x00, R11		; see if all key bytes have been used
		jeq			resetkey
		jmp			counteradmin

resetkey:
		mov			R12, R5			; place original key in register
		mov.b		#numberKey, R11	; restart number of keys
		jmp 		counteradmin

done:
		pop			R5
		pop			R8
		pop			R10
		ret
;-------------------------------------------------------------------------------
;Subroutine Name: decryptCharacter
;Author:
;Function: Decrypts a byte of data by XORing it with a key byte.  Returns the
;           decrypted byte in the same register the encrypted byte was passed in.
;           Expects both the encrypted data and key to be passed by value.
;Inputs:
;Outputs:
;Registers destroyed:
;-------------------------------------------------------------------------------

decryptCharacter:
			xor.b		0(R5), R10	; xor key with message
            ret

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect    .stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
