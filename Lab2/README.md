#Lab 2 - Subroutines - "Cryptography"

## By Sharee Acosta

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Prelab](#prelab)
2. [Hardware](#hardware)
3. [Code](#code)
6. [Debugging](#debugging)
8. [Test/Results](#test/results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)

 
### Objectives or Purpose 
The purpose of the lab was to decrypt an encrypted message using a key.  The program runs through an array of encrypted bytes.  The key was XORed with the encrypted byte to get an ASCII character.  After the program ran through the array, the hidden message was revealed.


### Prelab
The prelab consisted of a flowchart of the logic to run the program.  It consist of a main and 2 subroutines.  The main has the 2 subroutines within it.  It also pushes the information needed to decrypt the message.  The decrypt message subroutine uses decypt character within it.  They cycle back through each other until the array count is zero.  At that point, all the information needed to decrypt the message is popped back out of the stack.  Then it is program is terminated in a CPU trap.

![](http://i.imgur.com/PyhNMoj.jpg)
##### Figure 1: Prelab flowchart of the main portion of the program.  It contains both of the subroutines and the push and pop of the useful information to decrypt the message.  It also has the return function and CPU trap.

![](http://i.imgur.com/wEyWKzy.jpg)
##### Figure 2: Prelab flowchart of the subroutine of decrypting the message of the program.  This subroutine calls the decrypt character function.  It has the counter of the number of bytes of the message.  It also has the counter for the number of bytes of the key for B functionality.

![](http://i.imgur.com/V9s7Xd4.jpg)
##### Figure 3: Prelab flowchart of the subroutine of decrypting the character of the program.  This subroutine takes the byte of the message and the key and XORs them.  It then returns to the subroutine of decrypt message.

### Hardware
The hardware for this lab had a RAM and ROM limitation.  The results were stored in RAM and the tested array was stored in ROM.

### Code
There was not any specific code that I had to create to make a functionality work.  I used the push and pop function to get the information needed to decrypt the encrypted message.  I used two call functions to xor the encrypted byte and key.


### Debugging
I had a difficulty getting the program to place the decrypted message and information in the correct register.  I was not careful when assigning my registers.  It took me a while to find that I was trying to place the results of the XORed value in original myMessage register than the new register I was placing myResults.  I was also trying to increment myMessage and placing back into the same register rather than the new register specific for the counter.
A problem I had during B functionality was that restarting my key.  I had the counter working fine but I was "mov" rather than "mov.b" so it was placing 0x00 as my new key when I restarted the key.

### Test/Results
The required functionality had the program decrypt the encrypted message with a one byte key.  It required running through the program and incrementing myMessage and myResults as well as decrementing myMessage byte count.  It took the current byte of myMessage and XORed it with the given key to place in myResults.  The array for myMessage was 0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f.  It was 81 bytes long (0x5E).  The given key was 0xAC.  IT was XORed with every byte of the encrypted message to come with the message below.

![](http://i.imgur.com/OC0Tnw4.jpg)
##### Figure 4: The memory browser shows the encrypted message that was decrypted using the given key.

The B functionality had the program decrypt the encrypted message with a three byte key.  It required running through the program and incrementing myMessage and myResults as well as decrementing myMessage and key byte count.  It took the current byte of myMessage and XORed it with the given key according to key count to place in myResults.  The array for myMessage was 0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50.  It was 51 bytes long (0x51).  The given key was 0xACDF23.  It was XORed with every byte of the encrypted message to come with the message below.

![](http://i.imgur.com/ND5QFwm.jpg)
##### Figure 5: The memory browser shows the encrypted message that was decrypted using the given three byte key.

I did not accomplish the A functionality.  I attempted to find the key by looking at the most common bytes (unfortunately that file was deleted).  I tried to make the assumption that the most common bytes in the message were vowels.  Another approach was discussed in class to create another program to test if the key 0x00 was XORed with the 1st, 3rd and 5th byte of myMessage and it came out as A-z, spaces or periods then it was a possible key.  I would do the same with the 2nd, 4th and 6th byte of myMessage.  I would use a counter and loops to place the "working" key into the results.  The counter would run through and test the bytes until it reached the value of 0xFF.  However I was unable to get the program to work before the deadline.

I created my own test.  I chose a message to encrypt and a key to work with.  Then I worked backwards to XOR the encrypted message with the key I created.  The key I used was 0x03, 0x55, 0x5A, 0xFF.  The message I wanted to encrypt was "Hi, my name is Sharee Acosta!!"  I used the concept of the XORing the decrypted message with my key.  I worked it out on paper first.

![](http://i.imgur.com/a474AzF.jpg)
##### Figure 6: The hand calculations show 3 rows.  The top row is the ascii character for the letter below in the 2nd row.  The bottom row is the XORed value of the top row and the key underneath the character.

The encrypted message was 0x4b,0x3c,0x76,0xdf,0x6e,0x2c,0x7a,0x91,0x62,0x38,0x3f,0xdf,0x6a,0x26,0x7a,0xac,0x6b,0x34,0x28,0x9a,0x66,0x75,0x1b,0x9c,0x6c,0x26,0x2e,0x9e,0x22,0x74.

![](http://i.imgur.com/u77ehf8.jpg)
##### Figure 7: The memory browser shows the encrypted message of "Hi, my name is Sharee Acosta!!"

### Observation and Conclusions
The purpose of the lab was to decrypt an encrypted message using a key.  I was able to decrypt the encrypted message for 2/3 functionalities.  I was enable to decrypt the message without the given key.  However, I still learned and understood the concept of how to create a program to accomplish the purpose of the lab.

After completing this program, I learned how to use push, pop, call and return functions.  It was interesting to see how the stack pointer moved and mix of increments and decrements of registers.  A cool thing I did was the restart of the key.  I moved the original key into a new register so it would not be compromised.  It was placed as the new key after the key count reached zero.

The results of the lab were confirmed by my own test.  My program ran exactly how I predicted it to work with my personal test.

In the future, I can use the concepts of this lab for sending hidden messages like the Zimmerman telegram during WWI for future world crises.  Or it can be as simple as sending messages to my fellow ECE major students messages that no one else at the academy can read!


### Documentation
C2C Yi helped me understand how to push and pop the information.  C2C Orikogbo helped me get the B functionality and debug the problem of changing my "mov" to "mov.b"

