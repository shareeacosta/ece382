/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 16 Nov - 21 Nov 2016
Course: ECE382
File: main.c
HW: Lab6

Purp: Controls robot using PWM and timer as well as controlled by an IR remote.

Doc: C2C Mireles helped me understand how changing the TACCR values affect the wheel speed and the set up for the required functionality.  C2C Hanson helped me debug my required functionality and A functionality.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h>
#include "lab6.h"

int8	newIrPacket = FALSE;
int16	packetData[48];
int32	irPacket;
int8	packetIndex = 0;


void requiredFunctionality(){				// moves robot automatically through all required functionality requirements
	__delay_cycles(2000000);
	forward();
	__delay_cycles(1000000);
	backward();
	__delay_cycles(1000000);
	right();
	__delay_cycles(1000000);
	left();
	__delay_cycles(1000000);
	slightRight();
	__delay_cycles(1000000);
	slightLeft();
	return;
}

void main(void)
{
    WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer
    initMSP();

	while (1) {

		if (newIrPacket){					// checks the IR packet and performs function according to packet
			switch (irPacket){
				case UP:
					forward();
					break;
				case DOWN:
					backward();
					break;
				case LEFT:
					left();
					break;
				case RIGHT:
					right();
					break;
				case ONE:
					requiredFunctionality();
					break;
				case PWR:
					stop();
					break;

			}
		}

	}
}

#pragma vector = PORT2_VECTOR							// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;								// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;					// these values determines which logic is active

	switch (pin) {										// read the current pin level
		case 0:											// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;						//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R

			// based on the value of TAR, this if statement determines whether the pulse is Data1 or Data0
			if ((pulseDuration >= minLogic1Pulse) && (pulseDuration <= maxLogic1Pulse)) {
				irPacket = (irPacket << 1) | 1;
			}
			else {
				irPacket = (irPacket << 1);
			}

			packetData[packetIndex++] = pulseDuration;	// store TAR in packetData
			TA0CTL &= ~MC_1;							// turn timerA OFF
			LOW_2_HIGH; 								// Set up pin interrupt on positive edge
			break;

		case 1:											// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0;									// clear TAR
			TA0CTL |= MC_1;								// turn timerA ON
			TA0CTL |= TAIE;								// turn timerA interrupt ON
			HIGH_2_LOW; 								// Set up pin interrupt on falling edge
			break;
	} 													// end switch

	P2IFG &= ~BIT6;										// Clear the interrupt flag to prevent immediate ISR re-entry

}														// end pinChange ISR

#pragma vector = TIMER0_A1_VECTOR						// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {

	TA0CTL &= ~MC_1;									// turn TimerA OFF
	TA0CTL &= ~TAIE;									// turn TimerA interrupt OFF
	packetIndex = 0;									// clear the packetIndex
	newIrPacket = TRUE;									// hooray!! we have a packet (set newPacket)
	TA0CTL &= ~TAIFG;									// clear TimerA flag
}

