/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 16 Nov - 21 Nov 2016
Course: ECE382
File: lab6.c
HW: Lab6

Purp: Each function used to control the robot.

Doc: C2C Mireles helped me understand how changing the TACCR values affect the wheel speed and the set up for the required functionality.  C2C Hanson helped me debug my required functionality and A functionality.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h>
#include "lab6.h"
#include <msp430g2553.h>

void initMSP(){
	P2DIR |= BIT1;                		// TA1CCR1 on P2.1
	P2SEL |= BIT1;                		// TA1CCR1 on P2.1
	P2OUT &= ~BIT1;
	TA1CTL |= TASSEL_2|MC_1|ID_0;       // configure for SMCLK

	P2DIR |= BIT4;                		// TA1CCR2 on P2.4
	P2SEL |= BIT4;                		// TA1CCR2 on P2.4
	P2OUT &= ~BIT4;


	P2DIR |= BIT2;
	P2SEL &= ~BIT2;                		// Left GPIO on P2.2
	P2OUT &= ~BIT2;

	P2DIR |= BIT3;
	P2SEL &= ~BIT3;                		// Right GPIO on P2.3
	P2OUT &= ~BIT3;

	P2DIR |= BIT0;
	P2SEL &= ~BIT0;                		// Left and right motor enable on P2.0
	P2OUT &= ~BIT0;


	TA1CCR0 = 1000;                		// set signal period to 1000 clock cycles (~1 millisecond)
	TA1CCR1 = 750;                		// set duty cycle to 75%
	TA1CCTL1 |= OUTMOD_3;       		// set TACCTL1 to Set / Reset mode

	TA1CCR2 = 750;             		   	// set duty cycle to 75%
	TA1CCTL2 |= OUTMOD_3;        		// set TACCTL2 to Set / Reset mode


	P2SEL &= ~BIT6;						// Set up P2.6 as GPIO not XIN
	P2SEL2 &= ~BIT6;					// Once again, this take three lines
	P2DIR &= ~BIT6;						// to properly do

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.6
	P2IE  |= BIT6;						// Enable P2.6 interrupt

	HIGH_2_LOW;							// check the header out.  P2IES changed.
	P1DIR |= BIT0 | BIT6;				// Set LEDs as outputs
	P1OUT &= ~(BIT0 | BIT6);			// And turn the LEDs off

	TA0CCR0 = 16000;					// create a 16ms roll-over period
	TA0CTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
	TA0CTL = ID_0 | TASSEL_2 | MC_1;	// Use 1:8 prescalar off SMCLK and enable interrupts

	_enable_interrupt();
}

void forward(){							// rotate both wheels forward (GPIO = 0)
	TA1CCR1 = 425;						// left wheel was a little faster than right
	TA1CCR2 = 450;
	P2OUT |= BIT0;						// enable both motors
	P2OUT &= ~BIT1;
	__delay_cycles(2000000);
	P2OUT &= ~BIT0;						// turn off enable
	P2OUT |= BIT1;
	return;
}
void backward(){						// rotate both wheels backward (GPIO = 1)
	TA1CCR1 = 550;
	TA1CCR2 = 550;
    P2OUT |= BIT2;
    P2OUT |= BIT3;
    P2OUT |= BIT0;
    __delay_cycles(2000000);
    P2OUT &= ~BIT0;
    P2OUT &= ~BIT2;
    P2OUT &= ~BIT3;
    return;
}

void left(){							// rotate left wheel backward (GPIO = 1), right wheel forward (GPIO = 0)
	P2OUT |= BIT2;
	P2OUT |= BIT0;
	__delay_cycles(500000);
	P2OUT &= ~BIT0;
	P2OUT &= ~BIT2;
	return;
}


void right(){							// rotate right wheel backward (GPIO = 1), left wheel forward (GPIO = 0)
	P2OUT |= BIT3;
	P2OUT |= BIT0;
	__delay_cycles(500000);
	P2OUT &= ~BIT0;
	P2OUT &= ~BIT3;
	return;
}
void slightLeft(){						// rotate left wheel backward (GPIO = 1), right wheel forward (GPIO = 0)
	TA1CCR1 = 550;						// wheel speeds changed
	TA1CCR2 = 450;
	P2OUT |= BIT2;
	P2OUT |= BIT0;
	__delay_cycles(150000);				// run function to be < 45 degrees
	P2OUT &= ~BIT0;
	P2OUT &= ~BIT2;
	return;
}

void slightRight(){						// rotate right wheel backward (GPIO = 1), left wheel forward (GPIO = 0)
	TA1CCR1 = 450;						// wheel speeds change
	TA1CCR2 = 550;
	P2OUT |= BIT3;
	P2OUT |= BIT0;
	__delay_cycles(150000);				// run function to be < 45 degrees
	P2OUT &= ~BIT0;
	P2OUT &= ~BIT3;
	return;
}

void stop(){
	P2OUT &= ~ BIT0;					// turn off enable to stop robot
	return;
}
