# Lab 6 - PWM - "Robot Motion"

## By: Sharee Acosta

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Prelab](#prelab)
2. [Hardware](#hardware)
3. [Logic Analyzer](#logic-analyzer)
3. [Code](#code)
6. [Debugging](#debugging)
8. [Test/Results](#test/results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)

 
### Objectives or Purpose 
This lab is designed to provide you with experience using the pulse-width modulation features of the MSP430.  You will need to program the MSP430 to generate pulse-width-modulated waveforms to control the speed / direction of your robot's motors.  In this lab, you will make your robot move forward, backwards, a small (< 45 degree) turn left/right, and a large (> 45 dgree) turn left/right.

1) **Required functionality:** Demonstrate movement forward, backward, a small (< 45 degree) turn left and right, and a large (> 45 dgree) turn left and right.  The robot should perform these movements sequentially, completely disconnected from a computer (no USB cord).

2) **A Functionality:** Control your robot with your remote!  You should use at least four different buttons on the remote: one each to indicate motion directions of forward, backward, left, and right.


### Prelab
1) Consider your hardware (timer subsystems, chip pinout, etc.) and how you will use it to achieve robot control. Which pins will output which signals you need? Which side of the motor will you attach these signals to? How will you use these signals to achieve forward / back / left / right movement? Spend some time here, as these decisions will dictate much of how difficult this lab is for you.

- I will use a Timer A subsystem.  I'm going to use P2.0 to enable both motors.  I will use P2.1 for the left motor PWM and P2.4 for the right motor PWM.  P2.1 will be my left motor directive (GPIO) and P2.3 will be my right motor directive (GPIO).  I'll attach the left motor to the left side of the chip and the right motor to the right side of the chip.

- I will achieve the forward movement by setting the GPIO to zero on both the left and right motors.  This way, they will rotate forward until given a further instruction.

- The robot will move backward by setting the GPIO to one on both the left and right motors.  This will cause the motor to rotate in the opposite direction.

- In order to move to the left, I will stop my robot and rotate it 90 degrees to the left.  I will rotate the right wheel forward and the left wheel backward.  So the right motor will have the GPIO set to zero and the left motor will have the GPIO set to one.  The PWM on both motors will be set to one.  From there, I will set the GPIO for the left motor back to zero and have both motors move forward.

- The same movement will apply for right movement.  I will rotate the right motor backward and the left motor forward.  The right motor will have the GPIO set to one and the left motor will have the GPIO set to zero.  The PWM on both motors will be set to one.  When the robot has rotated 90 degrees to the right, I will set the GPIO on the right motor back to zero and the robot will move forward.

2) Consider how you will setup the PWM subsytem to achieve this control. What are the registers you'll need to use? Which bits in those registers are important? What's the initialization sequence you'll need?

- I will set up the PWM subsytem by connecting it to the P2.1 and P2.4 ports for the left and right respectively.  It controls the percentage on how much power will be given to motor.


		P2DIR |= BIT1;
		P2SEL |= BIT1;
		P2OUT &=~ BIT1;
		TA1CTL |= TASSEL_2 | ID_0 | MC_1;
		TA1CCR0 = 1000;
		TA1CCR1 = 250;
		TA1CCTL1 = Outmod_7;
		P2DIR |= BIT4;
		P2SEL |= BIT4;
		P2OUT &=~ BIT4;
		TA1CCR2 = 250;
		TA1CCTL2 = Outmod_7;


3) Consider what additional hardware you'll need (regulator, motor driver chip, decoupling capacitor, etc.) and how you'll configure / connect it.

- Motor driver chip: I'll connect the motor driver chip on the breadboard as well as the MSP430.  The left and right motors will also be connected to the motor chip drive.  The schematic for how the motor driver chip will be connected is attached.  It will take a 5 V input and output a 12 V.

- Regulator: It takes 5 V and converts it into 3.3 V.  It will be connected to the MSP430 to power the chip.

- Decoupling capacitors: The decoupling capacitor is placed in between the power and ground.  It is used to reduce the spikes in the current and smooths out the distortion in the output.

4) Consider the interface you'll want to create to your motors. Do you want to move each motor invidiually (moveLeftMotorForward())? Or do you want to move them together (moveRobotForward())?

- I want to incorporate both motors individually and together.  I want to create a loop where the forward and backward movements to have the motors work together.  However, when the robot is directed to move left and right, I want to create a another loop to separate the two motors so that the motors can rotate in the direction of the robot. 

![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/RequiredFunctionality_zpsbbm58zcg.jpg)
##### Figure 1:  This is the flowchart for how the robot will move.

![schematic](http://i206.photobucket.com/albums/bb73/imissyouverymuch/SchematicImage_zps3prllbcz.jpg)
##### Figure 2:  The image displays the schematic created by Fritznig.

### Hardware
The hardware for the lab was an MSP430, motor drive chip, IR sensor, voltage regulator and the robot motor.

![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/IMG_5172_zpsahwntsbf.jpg)
##### Figure 3:  The image shows my robot, Daaaave, with all the wire hooked up.


### Code
![](http://i206.photobucket.com/albums/bb73/imissyouverymuch/AFunctionality_zpsnkrnq9co.jpg)
##### Figure 4:  This flowchart has the added A functionality.

The required functionality focused on how each motor functioned on each movement.  In order to move forward, both of the wheels rotated forward with the GPIO off.  While running the function, I realized the left wheel rotated faster than the right.  In order to even out the wheels (so it went fairly straight), I decreased the TA1CCR1 value.  For the robot to move backward, I turned on the GPIO on for both motors to rotate backward.  Luckily, the wheels rotated at the same rate.  In order to turn right, I turned the GPIO on for the left motor and left it off for the right motor to turn the robot in a sharp movement.  I did the opposite code for the left movement.  For the turns less than 45 degrees, I changed the TACRR values to decrease the speed as well as increase the delay time to fit the requirements of the lab.


	void forward(){							// rotate both wheels forward (GPIO = 0)
		TA1CCR1 = 425;						// left wheel was a little faster than right
		TA1CCR2 = 450;
		P2OUT |= BIT0;						// enable both motors
		P2OUT &= ~BIT1;
		__delay_cycles(2000000);
		P2OUT &= ~BIT0;						// turn off enable
		P2OUT |= BIT1;
		return;
	}
	void backward(){						// rotate both wheels backward (GPIO = 1)
		TA1CCR1 = 550;
		TA1CCR2 = 550;
	    P2OUT |= BIT2;
    	P2OUT |= BIT3;
    	P2OUT |= BIT0;
    	__delay_cycles(2000000);
    	P2OUT &= ~BIT0;
    	P2OUT &= ~BIT2;
    	P2OUT &= ~BIT3;
    	return;
	}

	void left(){							// rotate left wheel backward (GPIO = 1), right wheel forward (GPIO = 0)
		P2OUT |= BIT2;
		P2OUT |= BIT0;
		__delay_cycles(500000);
		P2OUT &= ~BIT0;
		P2OUT &= ~BIT2;
		return;
	}


	void right(){							// rotate right wheel backward (GPIO = 1), left wheel forward (GPIO = 0)
		P2OUT |= BIT3;
		P2OUT |= BIT0;
		__delay_cycles(500000);
		P2OUT &= ~BIT0;
		P2OUT &= ~BIT3;
		return;
	}
	void slightLeft(){						// rotate left wheel backward (GPIO = 1), right wheel forward (GPIO = 0)
		TA1CCR1 = 550;						// wheel speeds changed
		TA1CCR2 = 450;
		P2OUT |= BIT2;
		P2OUT |= BIT0;
		__delay_cycles(150000);				// run function to be < 45 degrees
		P2OUT &= ~BIT0;
		P2OUT &= ~BIT2;
		return;
	}

	void slightRight(){						// rotate right wheel backward (GPIO = 1), left wheel forward (GPIO = 0)
		TA1CCR1 = 450;						// wheel speeds change
		TA1CCR2 = 550;
		P2OUT |= BIT3;
		P2OUT |= BIT0;
		__delay_cycles(150000);				// run function to be < 45 degrees
		P2OUT &= ~BIT0;
		P2OUT &= ~BIT3;
		return;
	}


For the A functionality, the main focus was to check the IR packet to see which function to use related to the button.  It checks the value of the button to see if it was pushed and perform the movement assigned to that button.



	if (newIrPacket){					// checks the IR packet and performs function according to packet
			switch (irPacket){
				case UP:
					forward();
					break;
				case DOWN:
					backward();
					break;
				case LEFT:
					left();
					break;
				case RIGHT:
					right();
					break;
				case ONE:
					requiredFunctionality();
					break;
				case PWR:
					stop();
					break;

		
### Debugging
There was a lot of debugging involved with this lab.  During the required functionality, I forgot to set P2DIR on each of the ports I was using.  The result of that nothing worked.  Oddly enough, when the enable and PWM of the left motor was removed, both the motors ran.  More debugging I did was changing the TACRR values to change the speed of the motor.  I also changed the delay times to run the specific function for a given time to meet the required specifications (e.g. < 45 degrees).  I included delays in between each function so that each movement was distinct and I was able to identify the rotation of the wheels.

For the A functionality, the remote was not working.  I had to change the average logic pulse values because the clock frequency was decreased.  I had to look at the packet values again and change the average values.  Another cool thing I added (not really a debug) was a stop function.  It really helped controlling the robot.  It order to do that, I just turned off the enable for both motors by clearing P2.0.


### Test/Results
I was able to successfully complete the required functionality.  The robot moved in the sequence of forward, backward, turn right, turn left, turn slight right and turn slight left.  The video displays a successful sequence. [https://youtu.be/vUP06YT5OgE](https://youtu.be/vUP06YT5OgE "requiredFunctionality")

I was also able to successfully complete A functionality.  I demonstrate all of the movements of the robot using the remote and IR sensor.  The video displays the robot movements. [https://youtu.be/NPXI2ZqE15U](https://youtu.be/NPXI2ZqE15U "AFunctionality")

### Observation and Conclusions
The purpose of the lab was to create a design with a pulse-width modulation features of the MSP430.  The MSP430 was used to generate pulse-width-modulated waveforms to control the speed / direction of the robot's motors.  In this lab, the robot will move forward, backwards, a small (< 45 degree) turn left/right, and a large (> 45 dgree) turn left/right.  The purpose was also to use a remote to control the robot using an IR sensor.  I was able to fulfill the purpose of the lab.

I learned a lot of things during this lab.  I learned more about how to assign the ports as well as set and clear them all.  I learned more about the different chips and the functions of them.  Even though as ECE majors, I got more experience with wiring the bread boards.  Because this was an individual lab, it forced me to do more research on what to do.  I also had more experience with the IR sensor and how it uses the data packets to function.

In the future, I can use this to control other items with the remote now that I understand the use of the IR sensor and data packets.  Knowing how the future labs will work, I can use my knowledge of sensors to automatically control the robot so it stops running into objects (which keeps happening).  Come to think of it, it will work similar to BB-8's toy function where it is able to identify and remember where the walls of a room are.  I can eventually create a robot with that function then I can sell the next Star Wars robot!


### Documentation
C2C Yi helped explain to me how the function for turning left and right worked as well as reversing the robot.  C2C Mireles helped explain how the PWM worked.  He also helped me understand how changing the TACCR values affect the wheel speed and the set up for the required functionality.  C2C Hanson helped me debug my required functionality and A functionality.

