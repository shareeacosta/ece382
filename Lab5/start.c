//-----------------------------------------------------------------
// Name:	Sharee Acosta and Anthony Talosaga
// File:	start.c
// Date:	2 Nov - 8 Nov 2016
// Purp:	Demo the decoding of an IR packet
//-----------------------------------------------------------------
#include <msp430g2553.h>
#include "start.h"
#include "ball.h"

int8	newIrPacket = FALSE;
int16	packetData[48];
int32	irPacket;
int8	packetIndex = 0;

extern void initMSP();
extern void initLCD();
extern void clearScreen();
extern void Delay160ms();
extern void Delay40ms();
extern void drawBox(int x, int y, int color);
extern void drawPaddle(int x, int y, int color);
extern void setBright(unsigned int x);

#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define		MAIN_BUTTON		(P1IN & BIT3)
#define		SCREEN_HEIGHT	320	//Screen size from Lab3
#define		SCREEN_WIDTH	240
#define		RED				0xF800
#define 	ORANGE			0xFD20
#define 	YELLOW  		0xFFE0
#define		GREEN			0x07E0
#define		BLUE    		0x001F
#define		WHITE			0xFFFF
#define		PINK			0xFB56
#define		CLEAR			0x0000
#define		VELOCITY		2
#define		XSTART			10
#define		YSTART			10
#define		RADIUS			5

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void main(void) {
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	initMSP430();				// Set up MSP to process IR and buttons

	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();

	int mode = 1;
	int x = SCREEN_WIDTH / 2;
	int y = SCREEN_HEIGHT / 2;
	int velocity = 7;

	drawBox(x, y, GREEN);

	while(1) {
		if (mode == 2) {	//the "eraser mode"
			if (irPacket == UP) {
				Delay160ms();
				if (hitTop(y, 5) == FALSE) {
					drawBox(x, y, CLEAR);
					y -= velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (irPacket == DOWN) {
				Delay160ms();
				if (hitBottom(y, 5, SCREEN_HEIGHT) == FALSE) {
					drawBox(x, y, CLEAR);
					y += velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (irPacket == LEFT) {
				Delay160ms();
				if (hitLeft(x, 5) == FALSE) {
					drawBox(x, y, CLEAR);
					x -= velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (irPacket == RIGHT) {
				Delay160ms();
				if (hitRight(x, 5, SCREEN_WIDTH) == FALSE) {
					drawBox(x, y, CLEAR);
					x += velocity;
					drawBox(x, y, WHITE);
				}
			}
			if (MAIN_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				drawBox(x, y, GREEN);
				mode = 1;
			}
		}
		if (mode == 1) {	//the drawing mode
			drawBox(x, y, GREEN);

			if (newIrPacket) {
			if (irPacket == UP) {
				Delay160ms();
				if (hitTop(y, 5) == FALSE) {
					y = y - velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (irPacket == DOWN) {
				Delay160ms();
				if (hitBottom(y, 5, SCREEN_HEIGHT) == FALSE) {
					y = y + velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (irPacket == LEFT) {
				Delay160ms();
				if (hitLeft(x, 5) == FALSE) {
					x = x - velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (irPacket == RIGHT) {
				Delay160ms();
				if (hitRight(x, 5, SCREEN_WIDTH) == FALSE) {
					x = x + velocity;
					drawBox(x, y, GREEN);
				}
			}
			if (MAIN_BUTTON == 0) {
				Delay160ms();
				Delay160ms();
				Delay160ms();
				Delay160ms();
				drawBox(x, y, WHITE);
				mode = 2;
			}
			}
			newIrPacket = FALSE;
		}
	} //second_game while end
} // end main

// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 		// stop WD

//	BCSCTL1 = CALBC1_8MHZ;
//	DCOCTL = CALDCO_8MHZ;

	P2SEL &= ~BIT6;						// Set up P2.6 as GPIO not XIN
	P2SEL2 &= ~BIT6;					// Once again, this take three lines
	P2DIR &= ~BIT6;						// to properly do

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.6
	P2IE  |= BIT6;						// Enable P2.6 interrupt

	HIGH_2_LOW;							// check the header out.  P2IES changed.
	P1DIR |= BIT0 | BIT6;				// Set LEDs as outputs
	P1OUT &= ~(BIT0 | BIT6);			// And turn the LEDs off

	TA0CCR0 = 16000;					// create a 16ms roll-over period
	TA0CTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
	TA0CTL = ID_3 | TASSEL_2 | MC_1;	// Use 1:8 prescalar off SMCLK and enable interrupts

	_enable_interrupt();
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;			// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;	// these values determines which logic is active

	switch (pin) {					// read the current pin level
		case 0:						// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;	//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R

			// based on the value of TAR, this if statement determines whether the pulse is Data1 or Data0
			if ((pulseDuration >= minLogic1Pulse) && (pulseDuration <= maxLogic1Pulse)) {
				irPacket = (irPacket << 1) | 1;
			}
			else {
				irPacket = (irPacket << 1);
			}

			packetData[packetIndex++] = pulseDuration;	// store TAR in packetData
			TA0CTL &= ~MC_1;			// turn timerA OFF
			LOW_2_HIGH; 				// Set up pin interrupt on positive edge
			break;

		case 1:							// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0;					// clear TAR
			TA0CTL |= MC_1;				// turn timerA ON
			TA0CTL |= TAIE;				// turn timerA interrupt ON
			HIGH_2_LOW; 				// Set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

// -----------------------------------------------------------------------
//			0 half-bit	1 half-bit		TIMER A COUNTS		TIMER A COUNTS
//	Logic 0	xxx
//	Logic 1
//	Start
//	End
//
// -----------------------------------------------------------------------
#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {

	TA0CTL &= ~MC_1;	// turn TimerA OFF
	TA0CTL &= ~TAIE;	// turn TimerA interrupt OFF
	packetIndex = 0;	// clear the packetIndex
	newIrPacket = TRUE;	// hooray!! we have a packet (set newPacket)
	TA0CTL &= ~TAIFG;	// clear TimerA flag
}
