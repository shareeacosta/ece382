# Assignment 6 - Your First C Program

## By Sharee Acosta
 
![](http://i.imgur.com/2HJH8g4.jpg)
#### Figure 1: The figure shows the memory of the program.

Answers to the following questions in readme.md file:
	-Exactly where in memory on the microcontroller are your variables stored?
    -How do you know?
	    - The memory is stored in RAM.  It is placed in the stack and the figure shows the location of the memory which shows the values of RAM.

### Documentation
C2C Hanson helped me debug my code and C2C Hayden helped me understand the computation for the Fibonacci number.

