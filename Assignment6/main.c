/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 5 Oct - 6 Oct 2016
Course: ECE382
File: Your First C Program
HW: Assignment 7

Purp:
- Creates three unsigned chars (val1, val2, val3).  Initializes them to 0x40, 0x35, and 0x42.
- Creates three unsigned chars (result1, result2, result3).  Initializes them to 0.
- Checks each number against a threshold value of 0x38.
- If val1 is greater than the threshold value, stores the [10th Fibonacci number](http://en.wikipedia.org/wiki/Fibonacci_number) in result1 by using a for loop.
- If val2 is greater than the threshold value, stores 0xAF to result2.
- If val3 is greater than the threshold value, subtracts 0x10 from val2 and stores the result into result3.

Doc:    C2C Hanson helped me debug my code and C2C Hayden helped me understand the computation for the Fibonacci number.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/


#include <msp430.h> 
#define threshold 0x38		// keep the value of threshold constant


/*
 * main.c
 */
	void main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
/*
 * �Creates three unsigned chars (val1, val2, val3). Initializes them to 0x40, 0x35, and 0x42.
�Creates three unsigned chars (result1, result2, result3). Initializes them to 0.
�Checks each number against a threshold value of 0x38.
�If val1 is greater than the threshold value, stores the 10th Fibonacci number in result1 by using a for loop.
�If val2 is greater than the threshold value, stores 0xAF to result2.
�If val3 is greater than the threshold value, subtracts 0x10 from val2 and stores the result into result3.

Additional Requirements:
�The threshold value must be a properly defined constant (refer to the lesson notes if you are confused by this requirement).
�Comment your code. In particular, provide a good file header (example provided in lesson notes).
 *
 */

    unsigned char val1, val2, val3;				// define variable and its value
    val1 = 0x40;
    val2 = 0x35;
    val3 = 0x42;

    unsigned char result1, result2, result3;	// define variable and its value
    result1 = 0;
    result2 = 0;
    result3 = 0;

   	int first = 1;								// define variables and its value
   	int second = 1;
   	int i =1 ;
   	int counter = 10;

   	if (val1 > threshold)						// compare if value1 is greater than 0x38
   	{
   		for (i;  i < counter-2 ; i++)			// if the number of loops is less than the counter (to include the numbers already used), go through the loop and increment the loop
   		{
   			result1 = first + second;			// add the first and second values
   			first = second;						// place the second operand in the first operand spot
   			second = result1;					// place the result in the second operand spot
   		}
   	}


	if (val2 > threshold)						// compare if value2 is greater than 0x38
	{
		result2 = 0xAF;							// if it is, place 0xAF in result2
	}

	if (val3 > threshold)						// compare if value3 is greater than 0x38
	{
		result3 = val2 - 0x10;					// if it is, place value2 - 0x10 into result3
	}

	while(1);									// CUP TRAP
}
