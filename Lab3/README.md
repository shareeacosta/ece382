#Lab 3 - SPI - "I/O"

## By Sharee Acosta

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Prelab](#prelab)
2. [Hardware](#hardware)
3. [Logic Analyzer](#logic-analyzer)
3. [Code](#code)
6. [Debugging](#debugging)
8. [Test/Results](#test/results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)

 
### Objectives or Purpose 
The purpose of the lab was to understand using the LCD BoosterPack with the MSP430.  In order to do that, the prelab consisted of creating a delay subroutine, understanding the LCD BoosterPack schematic, configuring the MSP430, and communicating with the LCD.  The lab consisted of understanding the physical communication on the logic analyzer, writing modes and drawing a square on the LCD with the ability to move it left, right, down or up.


### Prelab
The prelab consisted of creating a delay subroutine, understanding the LCD BoosterPack schematic, configuring the MSP430, and communicating with the LCD.  In order to create a delay subroutine of 160 ms, the assumption was made that the clock rate was 8 MHz.  With that assumption, the number of cycles needed for a 160 ms delay was found.

![](http://i.imgur.com/lzZ6b1M.jpg)
##### Figure 1: The code has comments for the amount of cycles each line is worth.  The code has 2 loops with a 10 count counter because there is more than 4 bytes worth of cycles.  The math shows the calculation for the hex value needed for the amount of loops through to reach the calculated amount of cycles.

The next portion of the prelab was to understand the schematic of the LCD BoosterPack.  It is necessary to have knowledge of the ports that connect to the MSP430.  Each signal and register has it's own value to be configured a certain way.

![](http://i.imgur.com/1QrgtUf.jpg)
##### Figure 2: The top tables show the correct pin number and the port it corresponds to.  The bottom table shows the values that are needed for each register to be configured properly.

The MSP430 also had to be configured for the ports used on the LCD BoosterPack.  

![](http://i.imgur.com/H6wGbFH.jpg)
##### Figure 3: The table shows where the pin is located for each port and its function.

Based off the pin configuration code, the code had to be described to properly configure the SPI subsytem.

![](http://i.imgur.com/2VmXefS.jpg)
##### Figure 4: The table shows which pin is needed for each function.

The description of the code is below.

Line 1: Setting the UCSWRST bit in the CTL1 register resets the subsystem into a known state until it is cleared. 
Line 2: Place the values of UCCKPH, UCMSB, UCMST, and UCSYNC into UCB0CTL0
Line 3: The UCSSEL_2 setting for the UCB0CTL1 register has been chosen, selecting the SMCLK (sub-main clock) as the bit rate source clock for when the MSP 430 is in master mode. 
Line 4: Set UCB0BR0 to 1
Line 5: Clear UCB0CTL1 0*256+1= scalar of 1 (keeps same clock)
Line 6: OR the 3 values and place in P1sel
Line 7: OR the 3 values and place in P1sel2
Line 8: USCI reset released for operation

Based off of the Write Command and Write Data, two timing diagrams were drawn.  It shows the expected behavior of the LCD_CS_PIN, LCD_DC_PIN, LCD_SCLK_PIN, and UCBxTXBUF.

![](http://i.imgur.com/RoETYeo.jpg)
![](http://i.imgur.com/cxaBsRd.jpg)
##### Figure 5: The two timing diagrams show the information changed and captured.

Finally, a table was filled out to describe the purpose of certain subroutines based off of the code.

![](http://i.imgur.com/RXuw2m5.jpg)
##### Figure 6: The table describes the purpose of each subroutine.

### Hardware
The hardware for this lab was the MSP430 and the LED BoosterPack.

### Logic Analyzer
First the clock was measured for it's exact period.  During the prelab, there was an assumption that the clock rate was 8 MHz.  The clock period was measured using a logic analyzer.  It was connected to P1.4 and grounded.  The clock period was 98 ns.

![](http://i.imgur.com/4CHRpxc.jpg)
##### Figure 7: The figure shows the clock wave form.  The wave period is displayed by subtracting the time from the one period on the rising edge (M2-purple and M1-green) which results in 98 ns.

After finding the clock period, delay loop had to be recalculated.  However, there was a problem with the delay loop.  The code did not run because a value kept replacing the counter resulting in a never ending loop.  Two lines were removed to allow for the code to run properly.  After the two lines were removed, a new loop counter was calculated.

![](http://i.imgur.com/wNUOCo6.jpg)
##### Figure 8: The calculations show a close cycle value to have the delay be for 160 ms.

However, when running the logic analyzer, the delay was too far off.  The code above has a call function and it was not necessary because the delay was a subroutine called into the program.  That eliminated 5 cycles from the delay.  It order to compensate for the cycle lost, the hex value was changed to increase the loop count.  The loop count was increased to 0xD4B9 to be within 0.1 ms of 160 ms.

![](http://i.imgur.com/sNuuHPj.jpg)
##### Figure 9: The figure shows the clock wave form for the delay code.  The wave period is displayed by subtracting the time from the one period on the rising edge (M2-purple and M1-green) which results in 159.92 ms.

After finding the delay, the code was ran to capture the waveform when the button S1 was pressed.  In order to attach the logic analyzer to the MSP430, the prelab was used.  Previously the pin number was found for the serial clock, LCD chip select, MOSI and DC.  The logic analyzer was used by attaching to the 2, 6, 7, and 15 port for CS, D, SCLK and MOSI respectively.  The waveform from connecting to these ports would give a waveform to understand the flow of the Write Command and Write Data subroutines.

![](http://i.imgur.com/wVK1LpH.jpg) 
##### Figure 10: The flowchart shows that there are 3 Write Commands and 8 Parameters (Write Data).  The flowchart shows what should be reflected in the waveform when S1 is pressed.

When the logic analyzer was connected to the MSP430 to the correct ports and the program was ran, the S1 button was pressed and the logic analyzer captured the waveform,

![Overview](http://i.imgur.com/e8ZqwvG.jpg)
##### Figure 11: The waveform shows the overview of the 11 packets from the S1 button being pushed.  Each packet is a part of the flowchart above.

Each packet was analyzed to understand what part of the flowchart it was and the values contained within it.  Based off of the flowchart, the first packet should be a Write Command followed by 4 Write Data's.  In the waveform the data is captured on the falling edge of the clock.  Write Command and Write Data are represented as 0 and 1 respectively in DC.  The value of the packet is defined in the MOSI waveform which is captured on the falling edge of the clock (SCLK).

![1](http://i.imgur.com/IyoKVVG.jpg)
##### Figure 12: The waveform has yellow lines which shows the 8 bits of the waveform which capture values on the falling edge.  The DC waveform shows a 1 which means it is a Write Command.  The MOSI waveform shows 00101010b which is 0x2A.  As the flowchart showed, the first packet was a Write Command which is used for the column address data.  It has 4 parameters (2 coordinate pairs) for the column address.

Next, there is supposed to be 4 Write Data's to fulfill the parameters in the flowchart.

![2](http://i.imgur.com/sg3LN5D.jpg)
##### Figure 13: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 00000000b which is 0x00.  This is the first parameter for the first coordinate and it takes the first byte of the column coordinate.  Since it is 0x00, the coordinate is at xStart.

![3](http://i.imgur.com/MhBB3ti.jpg)
##### Figure 14: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 01010101b which is 0x55.  This is the second parameter for the second coordinate and it takes the second byte of the column coordinate.  This coordinate makes sense because it is setting the area and it would need to move to the right from xStart.

![4](http://i.imgur.com/N0bWmEK.jpg)
##### Figure 15: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 00000000b which is 0x00.  This is the first parameter for the third coordinate and it takes the first byte of the column coordinate.  Since it is 0x00, the coordinate is at xStart.

![5](http://i.imgur.com/eBvNEqK.jpg)
##### Figure 16: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 01011010b which is 0x5A.  This is the second parameter for the fourth coordinate and it takes the second byte of the column coordinate.  This coordinate makes sense because it is setting the area and it would need to move to the right from xStart.

The four parameters have been set for the first Write Command and the flowchart indicated another Write Command followed by four parameters of Write Data.  It will have the same logic as the five previous wave forms but for the page (row) coordinates.

![6](http://i.imgur.com/7U8GUFp.jpg)
##### Figure 17: The waveform has yellow lines which shows the 8 bits of the waveform which capture values on the falling edge.  The DC waveform shows a 1 which means it is a Write Command.  The MOSI waveform shows 00101011b which is 0x2B.  As the flowchart showed, the sixth packet was a Write Command which is used for the page (row) address data.  It has 4 parameters (2 coordinate pairs) for the page address.

![7](http://i.imgur.com/xz1pNIW.jpg)
##### Figure 18: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 00000000b which is 0x00.  This is the first parameter for the first coordinate and it takes the first byte of the page coordinate.  Since it is 0x00, the coordinate is at yStart.

![8](http://i.imgur.com/ROIZOSU.jpg)
##### Figure 19: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 10110101b which is 0xB5.  This is the second parameter for the second coordinate and it takes the second byte of the page coordinate.  This coordinate makes sense because it is setting the area and it would need to move to down from yStart.

![9](http://i.imgur.com/sTu7H5m.jpg)
##### Figure 20: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 00000000b which is 0x00.  This is the first parameter for the third coordinate and it takes the first byte of the page coordinate.  Since it is 0x00, the coordinate is at yStart.

![10](http://i.imgur.com/EfUlC0t.jpg)
##### Figure 21: The DC waveform shows a 0 which means it is a Write Data.  The MOSI waveform shows 10110101b which is 0xB5.  This is the fourth parameter for the second coordinate and it takes the second byte of the page coordinate.  This coordinate makes sense because it is setting the area and it would need to move to down from yStart.

Both x and y coordinates have been set with the Write Command and Write Data.  The area is now set with the four coordinate pairs.  The last part of the flowchart to set the area was a Write Command.  Now that all the coordinates have been set, the are can be set.

![11](http://i.imgur.com/kg2mJ2L.jpg)
##### Figure 22: The DC waveform shows a 1 which means it is a Write Command.  The MOSI waveform shows 00101100b which is 0x2B.  This the final portion of the flowchart.  The purpose of this Write Command was to turn on the display with the set area of the four coordinates.

All of the waveforms have been interpreted into the meaning and the data is separated below.

![](http://i.imgur.com/cYAMbR4.jpg)
##### Figure 23: The table shows all of the converted data from the waveforms from above.

Another part of the logic analyzer was to understand the AND, OR and XOR operations when "coloring" in pixels.  Black pixels and white pixels represent 1 and 0 logic respectively.

![](http://i.imgur.com/u5ZsDMI.png)
##### Figure 24: The figure shows the relationship of AND, OR and XOR that will be applied on the LCD screen.  The operations were calculated to obtain the column on the far right.

### Code
For the functionality portion of the lab, code was created to form a 10x10 box and then to have the ability to move the box up, down, left and right.  In order to do this, the code for drawPixel was manipulated to create a 10x10 box.  The drawBox code called drawPixel to draw a pixel 10 times across, move down one row and repeat until 10 rows were drawn.

![](http://i.imgur.com/OgOUHgk.jpg)
##### Figure 25: The code above shows how drawPixel was used to draw a 10x10 box.  100 pixels were drawn to create the box.

In order to move the box, the coordinate of the box was either moved right, left, up or down 10 pixels.  The original box then was cleared from the screen.  After the clearscreen, the new box was drawn to appear as if it was moved in a certain direction.  The box was also prevented from moving past the boundaries.  If the box hit the screen boundaries, the original box was cleared and the new box was drawn directly over it.

![](http://i.imgur.com/RTUR175.jpg)
![](http://i.imgur.com/92LcXbX.jpg)
##### Figure 26: The code above shows how the box was moved using clearScreen and drawBox as a way to move the 10x10 box in a certain direction.  The buttons were coded to move the box in a certain direction.  S2 moved the box to the left, S3 moved the box down, S4 moved the box up and S5 moved the box to the left.

### Debugging
I had to debug a lot of problems.  The first problem I ran into was drawing the box.  My code wouldn't move down one row or restart the 10 counter.  It drew a diagonal line.  In order to fix that, I added a subtract function and I had to move the 10 counter value back into the column resister.  Another problem I ran into was that my box wouldn't move.  I had C2C Hanson look at my code and he couldn't find a problem with it either.  It wasn't until C2C Mireles looked at my code and found a simple mistake.  I was using bit set when I was preparing each button for polling instead of bit clear.  As soon as I changed that simple "s" to a "c," my program ran perfectly!

### Test/Results
The results of the functionality was successful.  I was able to draw a 10x10 box using the drawPixel function.  It ran a little slow because it had to draw 100 pixels in a 10x10 formation but it works.  The link below is a video that displays both the required functionality and A functionality.
[https://youtu.be/DabkfW9OTWE](https://youtu.be/DabkfW9OTWE "Lab3Functionality")

### Observation and Conclusions
This program draws a 10x10 box using a subroutine of drawPixel (drawing 100 pixels) and moves the box up, down, left and right.  It also had a lot of understanding the usage of the LCD BoosterPack and how it communicates with the MSP430.  I was able to fulfill the purpose of the lab.  I demonstrated the waveforms and data within it when the button S1 was pushed.

After completing this program, I learned a more than I expected.  For starters, I enhanced my learning on reading schematics and how the slaves are connected to their masters.  Within that, I learned how the LCD BoosterPack connects to the MSP430 which is very interesting.  I was very proud to learn how the function and purpose of each port.  I also learned that I need more patience with the lab.  I got very frustrated (sorry when I wasn't the nicest person during night EI) because there were many parts to this lab that I was not doing in order which made it difficult.  However, with help, I was able to accomplish the lab.

The video shows that I accomplished the functionalities of the lab.

In the future, I can use the concepts of this lab to draw cool things.  You could probably play snake on here?  Now that I have that thought, I'd like to try and create that game one day.  You can use operations like OR, AND, and XOR to override some of the pixels (interesting though).  It's pretty cool that we were exposed to drawing on the LCD because it's the basics of video games!

### Documentation
C2C Hanson explained how to find the numbers for the tables.  C2C Talasaga and C2C Kim helped me with a math error on my delay.  C2C Hanson also explained how to manipulate the drawPixel to create a box and moving the box.  C2C Mireles helped me debug my code to move my box.  C2C Yi and C2C Orikigbo also helped me understand how to use the logic analyzer.

