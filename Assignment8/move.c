/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 13 - 16 October 2016
Course: ECE 382
File: move.c
Event: Assignment 8 - Moving Average

Purp: Functions to update and monitor a moving average

Doc: C2C Mireles helped me with debugging and the correct coding for the functions.  C2C Yi helped explain the addSample function and how it was supposed to work.  He also helped me debug my code.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

/*------------------------------------------------------------------------/
* getAverage
* Sharee Acosta
* Finds the average of the the number of samples of the array
* Inputs
* 	newArray, N_AVG_SAMPLES
* Outputs
* 	average = sum/N_AVG_SAMPLES
*/
int getAverage(int newArray[], int N_AVG_SAMPLES)
{
	int	sum = newArray[0];					// first of the array is the initial sum
	int i;
	for(i = 1; i < N_AVG_SAMPLES; i++)		// increase counter until it is less than the number of samples and add the array value to the sum
	{
		sum += newArray[i];
	}

	return	sum/N_AVG_SAMPLES;
}

/*------------------------------------------------------------------------/
* addSample
* Sharee Acosta
* Takes the array values in orders and places them in a new array while shifting the values
* Inputs
* 	newArray, N_AVG_SAMPLES, array, arrayLength
* Outputs
* 	newArray
*/
void addSample(int newArray[], int array[], unsigned int arrayLength, int N_AVG_SAMPLES)
{
		volatile int i;
		for(i = 0; i < N_AVG_SAMPLES; i++)				// increase counter until it is less than the number of samples and shift value
		{
			newArray[i] = newArray[i + 1];
		}

		newArray[N_AVG_SAMPLES - 1] = array[0];
		for(i = 0; i < arrayLength - 1; i++)			// increase counter until it is less than the number of samples and shift value
		{
			array[i] = array[i + 1];
		}

		return;
}

// Array functions
/*------------------------------------------------------------------------/
* max
* Sharee Acosta
* Finds the maxiumum value in the array
* Inputs
* 	array, arrayLength
* Outputs
* 	max
*/
int max(int array[], unsigned int arrayLength)
{
	int max = 0;
	int i;
	for(i = 0; i < arrayLength; i++)	// if the counter is less than the array length, compare the array value to the max value and set it to max if it is
	{
		if(array[i] > max)
		{
			max = array[i];
		}
	}

	return	max;
}

/*------------------------------------------------------------------------/
* min
* Sharee Acosta
* Finds the minimum value in the array
* Inputs
* 	array, arrayLength
* Outputs
* 	min
*/
int min(int array[], unsigned int arrayLength)
{
	int min = array[0];
	int i;
	for(i = 0; i < arrayLength; i++)	// if the counter is less than the array length, compare the array value to the min value and set it to min if it is
	{
		if(array[i] < min)
		{
			min = array[i];
		}
	}

	return	min;
}

/*------------------------------------------------------------------------/
* range
* Sharee Acosta
* Finds the range of the array
* Inputs
* 	min, max
* Outputs
* 	range = max - min
*/
unsigned int range(int min, int max)
{

	return	max - min;
}

