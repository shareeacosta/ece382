# Assignment 8 - Moving Average

## By Sharee Acosta

Array 1: 45, 42, 41, 40, 43, 45, 46, 47, 49, 45
Array 2: 174, 162, 149, 85, 130, 149, 153, 164, 169, 173

### Array 1 with 2 average samples
![](http://i.imgur.com/b0WZL5v.jpg)
#### Figure 1: The screen shot shows the average between the 2 samples values of the array starting at x=2.  It also shows the max, min and the range.

### Array 1 with 4 average samples
![](http://i.imgur.com/mn6kUr9.jpg)
#### Figure 2: The screen shot shows the average between the 4 samples values of the array starting at x=4.  It also shows the max, min and the range (which is the same as figure 1).

### Array 2 with 2 average samples
![](http://i.imgur.com/lc7kLAI.jpg)
#### Figure 3: The screen shot shows the average between the 2 samples values of the array starting at x=2.  It also shows the max, min and the range.

### Array 2 with 4 average samples
![](http://i.imgur.com/lsu11Tf.jpg)
#### Figure 4: The screen shot shows the average between the 4 samples values of the array starting at x=4.  It also shows the max, min and the range (which is the same as figure 3).

Answers to the following questions in readme.md file:
	-What data type did you choose for your arrays? Why?
		I used integers because I wanted whole numbers and not floats.  It is also less memory.
	-How did you verify your code functions correctly?
		I verified my code functions correctly by performing hand calculations with each sample.  I also drew a picure for the addSample function to understand how it was supposed to function.


### Documentation
C2C Mireles helped me with debugging and the correct coding for the functions.  C2C Yi helped explain the addSample function and how it was supposed to work.  He also helped me debug my code.
