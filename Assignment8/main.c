/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 13 - 16 October 2016
Course: ECE 382
File: main.c
Event: Assignment 8 - Moving Average

Purp: Functions to update and monitor a moving average

Doc: C2C Mireles helped me with debugging and the correct coding for the functions.  C2C Yi helped explain the addSample function and how it was supposed to work.  He also helped me debug my code.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h> 
#define arrayLength 10
#define N_AVG_SAMPLES 2

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    // define and initialize variables
    int array[arrayLength] = {174, 162, 149, 85, 130, 149, 153, 164, 169, 173};
    volatile int averageArray[arrayLength + 1];
    int i;
    volatile int myRange;
    int newArray[N_AVG_SAMPLES] = {0};
    volatile int myMin = 0;
    volatile int myMax = 0;

    // set the max, min and range from the functions
    myMax = max(array, arrayLength);
    myMin = min(array, arrayLength);
    myRange = range(myMin, myMax);

    for(i = 0; i <= arrayLength; i++)		// find the moving average
    {
    	averageArray[i] = getAverage(newArray, N_AVG_SAMPLES);
    	addSample(newArray, array, arrayLength, N_AVG_SAMPLES);
    }

    while(1){;}		//CPU TRAP


}
