/*--------------------------------------------------------------------
Name: Sharee Acosta
Date: 13 - 16 October 2016
Course: ECE 382
File: move.h
Event: Assignment 8 - Moving Average

Purp: Functions to update and monitor a moving average

Doc: C2C Mireles helped me with debugging and the correct coding for the functions.  C2C Yi helped explain the addSample function and how it was supposed to work.  He also helped me debug my code.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef _MOV_AVG_H
#define _MOV_AVG_H

#define N_AVG_SAMPLES

// Moving average functions
int getAverage(int newArray[], int N_AVG_SAMPLES);
void addSample(int newArray, int array[], unsigned int arrayLength, int N_AVG_SAMPLES);

// Array functions
int max(int array[], unsigned int arrayLength);
int min(int array[], unsigned int arrayLength);
unsigned int range(int myMin, int myMax);

#endif
