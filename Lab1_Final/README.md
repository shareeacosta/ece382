#Lab 1 - Assembly Language - "A Simple Calculator"

## By Sharee Acosta

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Prelab](#prelab)
2. [Hardware](#hardware)
3. [Code](#code)
6. [Debugging](#debugging)
8. [Test/Results](#test/results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)

 
### Objectives or Purpose 
The purpose of the lab was to create a calculator.  Calculator operators were represented by byte values.  Add was 0x11, subtract was 0x22, multiply was 0x33, clear was 0x44, and multiply was 0x55.  After given an array, the program will read through the array and perform the calculations and store them in RAM.


### Prelab
The prelab consisted of a flowchart of the logic to run the program.  It consists of all the operands and the decisions the program will have to make.  I had to make multiple changes to the flowchart because I originally did not take into account if none of the operators occurred.  I also did not take into consideration values greater than 0xFF or less than 0x00.  The final adjustment I had to make was keeping an add, subtract or multiply result as the first operand.
![](http://i.imgur.com/ueWMFoC.jpg)
##### Figure 1: Prelab flowchart to show the logic of how the program will run.

### Hardware
The hardware for this lab had a RAM and ROM limitation.  The results were stored in RAM and the tested array was stored in ROM.  The results stored could not be greater than 0xFF or less than 0x00.

### Code
A specific code that I wrote was the Russian Peasant for the multiplication operation.  Instead of counting through how many times a value had to be added together, I used the Russian Peasant to shift my values as multiplication.

![](http://i.imgur.com/FF5JTKh.jpg)
##### Figure 2: The code show the multiplication operation loop to shift the bytes to multiply them then add those values together.

I had my code run grab the next byte in the array and check for the operator.  It compared for each operator value and jumped to that specific operator loop or ended completely if the operator was not found.  After the operation was completed, the code sent the result through conditions to see if the value was less than 0x00 or greater than 0xFF.  If the value fell out of the range, it was automatically set to 0x00 or 0xFF depending if it was over or under the range.  Finally the code ended in a CUP trap.


### Debugging
I had a difficulty understanding how to grab the next value in the array.  I had to get help to understand how  to increment the count would place the next value in the array in the address.  I also had problems with the loop of values less than 0x00.  If the value started with an F, it automatically went to the less than 0x00 loop.  Instead of comparing the operand to 0x00, I compared the two operands.  The only time the value would be less than 0x00 would be in a subtract operator.  I compared the two operands to see if the first operand was less than the second operand.  If that was true, then the value would be less than 0x00.

### Test/Results
The required functionality had addition, subtraction, clear and end operations.  It was pulled from the array of 0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55.  Using hand calculations, the result should have been 0x22, 0x33, 0x00, 0x00, 0xCC.

![](http://i.imgur.com/4Gq4Si3.jpg)
##### Figure 3: The memory browser shows the replicated results from the hand calculation for the required functionality.

The B functionality incorporated results that were outside of the range of values allowed.  It had addition, subtraction, clear and end operations.  If the addition operation was greater than 0xFF, then the result was set to 0xFF.  If the subtraction operation was less than 0x00, then the result was set to 0x00.  It was pulled from the array of 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55.  Using hand calculations, the result should have been 0x22, 0x33, 0x44, 0xFF, 0x00, 0x00, 0x00, 0x02.

![](http://i.imgur.com/QMMLOOs.jpg)
##### Figure 4: The memory browser shows the replicated results from the hand calculation for the B functionality.  Specifically focus on the 4th and 5th result.  They both were set to the greatest and least value because they fell outside of the range.

The A functionality incorporated the multiplication operation.  It had addition, subtraction, multiplication, clear and end operations.  If the addition or multiplication operation was greater than 0xFF, then the result was set to 0xFF.  If the subtraction operation was less than 0x00, then the result was set to 0x00.  It was pulled from the array of 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55.  Using hand calculations, the result should have been  0x44, 0x11, 0x88, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x00, 0x00, 0xff.

![](http://i.imgur.com/OLQiZao.jpg)
##### Figure 5: The memory browser shows the replicated results from the hand calculation for the A functionality.  Specifically focus on the 3rd result which shows the correct multiplication value and 7th result is greater than the range.  The 9th result uses multiplication that results in a value greater than the range and the 10th result is a multiplication of an operand and 0 equaling 0.

I created 2 test to test the unknowns of the program.  One of the unknowns was what would happen if an unknown operator was placed in the program?  I used the array of 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x08, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55.  The 9th value in the array is not a valid operator.  Therefore, it should terminate the program at the 5th result.

![](http://i.imgur.com/ZwfXC3z.jpg)
##### Figure 6: The memory browser shows the replicated results from the hand calculation for the unknown operator.  Specifically focus on the 5th result.  Comparing it from the A functionality.  I changed the 9th value in the array which terminated the program instead of subtracting.

The second test I created was having unsigned numbers go through the program.  The program still runs with unsigned values.  I used the array of 0x88, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55.  I changed the first value of the required functionality array.  The hand calculated results should be 
0x99, 0xAA, 0x00, 0x00, 0xCC.

![](http://i.imgur.com/EuQk3vg.jpg)
##### Figure 7: The memory browser shows the replicated results from the hand calculation for unsigned operands.  I started the array with an unsigned value and it ran through the program properly.


### Observation and Conclusions
After completing this program, I learned how to use a register indirect and a post increment.  I also learned how to use the Russian peasant method to multiply.  It was a very interesting process to learn because it worked in a way that shifted the bits and added them together to essential get the multiplied value.

The results of the lab confirmed the hand calculated values of the arrays.  It worked for all 3 functionalities as well as unknown operators and unsigned operands.


### Documentation
I went in to Capt Faulkinburg's class.  C2C Kim helped me with understand the counter and how to grab from the array.

