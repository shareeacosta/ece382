;-------------------------------------------------------------------------------
; Sharee Acosta
; 6 Sep - 9 Sep 2016
; Capt Warner/T4
;
; This program acts as a calculator using addition, subraction, multiplication, clear and end operations.
;
; Documentation:  I went in to Capt Faulkinburg's class.  C2C Kim helped me with understand the counter and how to grab from the array.
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

myProgram:		.byte		0x88, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55	; defined in ROM
ADD_OP:			.equ		0x11
SUB_OP:			.equ		0x22
CLR_OP:			.equ		0x44
END_OP:			.equ		0x55
MUL_OP:			.equ		0x33

				.data						; assemble into RAM memory
myResults:		.space		20				; reserving 20 bytes of space

				.text						; assemble into program memory
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

           		mov			#myProgram, R4		; read program from memory (ROM)
				mov.b		#0x00, R11			; set value for less than 0x00 comparison
				mov.b		#0xFF, R12			; set value for greater than 0x00 comparison
				mov.b		#0x01, R13			; set value for counter on A functionality
				mov			#myResults, r10		; write results to memory (RAM)

restart:		mov.b		@R4+, R5			; read first operand and increment array

loop:
				mov.b		@R4+, R6			; read operator and increment array
				cmp.b		#ADD_OP, R6			; compare each operator to its byte value
				jeq			add
				cmp.b		#SUB_OP, R6
				jeq			sub
				cmp.b		#CLR_OP, R6
				jeq			clr
				cmp.b		#MUL_OP, R6
				jeq			mul
				jmp			end					; if none, go straight to end op

add:

				mov.b		@R4+, R7			; take second operand and increment array
				add			R7, R5
				jmp		 	result				; restart loop and grab operator

sub:
				mov.b		@R4+, R7			; take second operand
				cmp			R7, R5				; compare by subtracting the second operand from the first operand
				jl			lowresult			; if the second operand < first operand, it's negative and go to less than result
				sub.b		R7, R5
				mov.b		R5, 0(R10)			; write result into memory
				mov.b		@R10+, R5			; increment result array
				jmp			loop				; restart loop and grab next operator

clr:
				mov.b		@R10+, R5			; increment myResults
				mov.b		R11, 0(R10)			; place 0x00 in myResults
				jmp			restart				; go and read a new operand

mul:
				mov.b		@R4+, R7			; take second operand and increment array
mul_loop:		cmp			R13, R7				; count until second operand is 1
				jeq			result				; when counter is at 1, store the result
				cmp 		R11, R7				; see if operand is 0
				jeq			zeroresult
				rra			R7
				rla			R5
				add			R11, R5
				jmp			mul_loop			; repeat loop until Russian Peasant process is complete

zeroresult:		mov.b		R11, R5				; anything multipled by 0 is 0

result:			cmp			R12, R5				; see if result is greater than 0xFF
				jge			highresult			; if it is, go to highresult
				mov.b		R5, 0(R10)			; place the result in myResults
				mov.b		@R10+, R5			; increment myResults
				jmp			loop				; go to array and grab operator

highresult:
				mov.b		R12, 0(R10)			; place 0x00 in myResults
				mov.b		@R10+, R5			; increment myResults
				jmp			loop				; go to array and grab operator

lowresult:
				mov.b		R11, 0(R10)			; place 0x00 in myResults
				mov.b		@R10+, R5			; increment myResults
				jmp			loop				; go to array and grab operator
end:
				jmp			forever				; send straight to CUP trap

forever:		jmp			forever				; CUP trap
                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
